<?php

namespace Libraries;

class Helper{

	public function __construct(){}

	// UNTUK MENAMPILKAN MENU - SUBMENU NON UNLIMITED MENU
	public function generateSubMenu($data) {
	    foreach($data as $key => $cat) {
	    	foreach($data[0] as $k => $v) {
	    		if($data[$key][0]->parent_id == $data[0][$k]->id_kategori) {
	    			$data[0][$k]->childs = $data[$key];	
	    		}
	    		
	    	}
	    }
	    return $data[0];
	}

	public function showMenuTemplate($data) {

		function get_menu($data, $parent = 0) {
		  static $i = 1;
		  $tab = str_repeat(" ", $i);
		  if (isset($data[$parent])) {
			  $html = "$tab<ul class='sf-menu sf-vertical'>";
			  $i++;
			  foreach ($data[$parent] as $v) {
					$child = get_menu($data, $v->id_kategori);
					$html .= $tab . '<li>';
					$html .= '<a href="#">' . $v->nama_kategori . '</a>';
				   if ($child) {
					   $i--;
					   $html .= $child;
					   $html .= "$tab";
				   }
				   $html .= '</li>';
			  }
			  $html .= "$tab</ul>";
			  return $html;
		  } 
		else {
			  return false;
		  }
		}

		return get_menu($data);		

	}

	public function showMenu($data, $page = 'cat_admin_home') {

		function get_menu($data, $parent = 0, $page) {
		  static $i = 1;
		  $tab = str_repeat(" ", $i);
		  if (isset($data[$parent])) {
			  $html = "$tab<ul class='tree'>";
			  $i++;
			  foreach ($data[$parent] as $v) {
					$child = get_menu($data, $v->id_kategori);
					$html .= $tab . '<li id="idCategory-'.$v->id_kategori.'">';
					$html .= '<a href="./category/edit?id='.$v->id_kategori.'">' . $v->nama_kategori . ' ('.$v->jmlProduk.' Produk)</a>  <a data-url="'.$v->id_kategori.'" class="delCat" href="#"><i style="color: red;" class="fa fa-trash"> Hapus</i></a>';
				   if ($child) {
					   $i--;
					   $html .= $child;
					   $html .= "$tab";
				   }
				   $html .= '</li>';
			  }
			  $html .= "$tab</ul>";
			  return $html;
		  } 
		else {
			  return false;
		  }
		}

		return get_menu($data, $parent = 0, $page);	
	}

	public function showStandardMenuAdd($data) {
		// echo "<pre>"; print_r($data); echo "</pre>"; die;
		$html = "";
		foreach($data[0] as $v) {
			$html .= "<option value='".$v->id_kategori."'>".$v->nama_kategori."</option>";
		}
		return $html;
	
	}

	public function showUnlimitedMenuAdd($data) {

		function kategori($data, $parent = 0) {
			static $i = 1;
			$tab = str_repeat(" ", $i);
			static $a = 0;
			$pusher="";
			$showPusher = str_repeat($pusher,$a);
			if (isset($data[$parent])) {
				$html = "$tab";
				$i++;
				foreach ($data[$parent] as $v) {
					$a++;
					 $child = kategori($data, $v->id_kategori);
					 $html .= "$tab";
					 if ($v->parent_id==0) {
						$listChild="";
						// $html .= '<option class="parent" value="'.$v->id_kategori.'"><b>'.$showPusher.' '.$v->nama_kategori.'</b></option>';
						$html .= '<optgroup label="'.$v->nama_kategori.'">';
						}
					 
					 if($v->parent_id !=0) {
					 $html .= '<option value="'.$v->id_kategori.'">'.$showPusher.' '.$v->nama_kategori.'</option>';
					 }
					 
					 $a--;
					 if ($child) {
						 $i--;
						 $html .= $child;
						 $html .= "$tab";
					 }
					 $html .= '';
				}
				$html .= "$tab</optgroup>";
				return $html;
			} 
		  else {
				return false;
			}
		}
		return kategori($data);

	}

	public function setSeoTitle($name) {

        $name = str_replace(" ", "-", preg_replace( "/[^A-Za-z0-9?! ]/", "", $name) );
        $name = strtolower( ltrim(rtrim($name, "-")) );
    
        return $name;	
	
	}

    public function kirimEmail($to, $from, $subject, $text_body, $html_body) {

        $this->mailer   = new \Libraries\mailer($to, $from, $subject, $text_body, $html_body);

        $this->mailer->send();

        return true;

    }	
	
}