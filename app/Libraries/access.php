<?php

namespace Libraries;

class Access{

	public function __construct(){

		$this->session  = new \Resources\Session;
		$this->kontrol	= new \Resources\Controller;

	}

	public function accessLogin(){
		if( $this->session->getValue('isLogin') === true )
			return true;
		else
			return false;
	}
	
}