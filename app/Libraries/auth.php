<?php

namespace Libraries;

class Auth {

	public function __construct(){

		$this->session  = new \Resources\Session;
		$this->login 	= new \Models\User;

	}

    public function cekLogin($username, $password, $table = 'admins')
    {
        if($this->login->cekLogin($username, $password, $table)) {
            
            $this->session->setValue(
                array(
                    'isLogin' => true,
                    'username' => $username
                )
            );

            setcookie("isLogin", true);

            return TRUE;
        
        } else {
            return FALSE;
        }
    }
	
}