<?php
namespace Controllers;
use Resources, Models;

class Category extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();
    }

    public function alias ($name) {

        $name = stripslashes(strip_tags(htmlspecialchars($name,ENT_QUOTES)));
        $id = $this->category->cekId($name);

        $page = 1;

        $this->pagination = new Resources\Pagination();     

        $page  = (isset($_GET['page'])) ? (int)$_GET['page'] : (int)$page;
        $limit = $this->limit;

        $data['products']   = $this->product->readAllProduct($page, $limit, 'id_produk, nama_produk, nama_kategori, gambar, harga', $id);

        foreach($data['products'] as $key => $v) {
            $data['products'][$key]->gambar = unserialize($v->gambar);
            $data['products'][$key]->thumbnail = $data['products'][$key]->gambar[0];
        }

        $total = $this->product->myTotalProductCategory($id);

        if($total > $limit) {

            $data['pageLinks'] = $this->pagination
                        ->setOption(
                            array(
                                'limit'     => $limit,
                                'base'      => $this->baseConfig['root'].'category/'.$name.'/?page=%#%',
                                'total'     => $total,
                                'current'   => $page
                            )
                        )
                        ->getUrl();
        
        }

        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];          
        $data['categories'] = $this->baseConfig['categories'];
        $data['setting']    = $this->setting->read();
        $data['page']    = $this->page->readAllPage('publish');
        $data['listProduk'] = true;

        echo $this->template->render($data);    	

    }

}