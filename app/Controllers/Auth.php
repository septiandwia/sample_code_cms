<?php
namespace Controllers;
use Resources, Models;

class Auth extends Resources\ControllerBaru
{    

    public function __construct() {
        parent::__construct();
        $this->loginUser = new \Libraries\auth;
    }
    
    public function index()
    {    
    	echo "<pre>"; print_r("var"); echo "</pre>"; die;
    }

    public function login()
    {
    	if(isset($_POST['username']) AND isset($_POST['password'])) {

            $username = $this->request->post('username', FILTER_SANITIZE_STRING);
            $password = $this->request->post('password', FILTER_SANITIZE_STRING);
            
            if( $this->loginUser->cekLogin($username, md5($password), $table='admins') ) {
                $this->redirect('admin/dashboard');
            } else {
                $data['erMessage'] = "Username atau Password salah!";
                $this->output('admin', $data);                
            }

        } else {
            $this->redirect('admin');
        }
    }

    public function logout()
    {
        session_destroy();
    }

}