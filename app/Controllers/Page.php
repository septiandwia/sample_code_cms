<?php
namespace Controllers;
use Resources, Models;

class Page extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();
    }

    public function index() {
    	$this->redirect('home');
    }

    public function alias($name) {
	    
	    $name = stripslashes(strip_tags(htmlspecialchars($name,ENT_QUOTES)));

	    $data['categories'] = $this->category->readAllCategoryFront();
	    $data['categories'] = $this->helper->generateSubMenu($data['categories']);
	    $data['staticPage'] = true;
	    $data['p'] = $this->page->readPageSlug($name);
	    $data['page'] = $this->page->readAllPage('publish');
	    $data['staticPageTitle'] = $data['p'][0]->title;
	    $data['staticPageContent'] = $data['p'][0]->content;
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
	    $data['setting'] = $this->setting->read();

	    echo $this->template->render($data);

    }
}