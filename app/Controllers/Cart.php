<?php
namespace Controllers;
use Resources, Models;

class Cart extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();
    }

    public function index() {

    	if(isset($_POST['remove'])) {
    		$this->cart->remove($_POST['remove']);
    	} 

    	if(isset($_POST['quantity'])) {
    		foreach($_POST['quantity'] as $key => $val) {
    			$this->cart->update(array('rowid' => $key, 'quantity' => $val));
    		}
    	}

    	if(isset($_POST['id'])) {

    		$id_produk = (int)$_POST['id'];

	    	$data['product'] = $this->product->readProduct($id_produk, 'id_produk, berat, nama_produk, harga, gambar');
	    	$data['id'] 	= $data['product'][0]->id_produk;
	    	$data['name'] 	= $data['product'][0]->nama_produk;
	    	$data['image'] 	= unserialize($data['product'][0]->gambar);
	    	$data['image'] 	= $data['image'][0];
	    	$data['quantity'] = (isset($_POST['quantity'])) ? $_POST['quantity'] : 0;
            $data['price'] = $data['product'][0]->harga;
	    	$data['weight'] = $data['product'][0]->berat;

	    	unset($data['product']);

	    	$this->cart->insert($data);
    
            unset($data['product'], $data['name'], $data['image'], $data['price'], $data['quantity']);
        	
    	}

	    $data['categories'] = $this->baseConfig['categories'];
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
        $data['setting']    = $this->setting->read();
    	$data['cartItems'] = $this->cart->total_items();
    	$data['cartTotal'] = $this->cart->total();
    	$data['ShoppingCart'] = $this->cart->contents();
        $data['title'] = 'Your Cart';
        $data['page'] = $this->page->readAllPage('publish');
        $data['detailCart'] = true;

        if(!isset($_SESSION['ShoppingCart']))
        {
            $data['emptyCart'] = true;

        } else {
    
            if (!isset($_SESSION['ShoppingCart']) && $_SESSION['ShoppingCart']['total_items'] <= 0 || $_SESSION['ShoppingCart']['total_items'] == 0)
            $data['emptyCart'] = true;            
    
            else $data['emptyCart'] = false;

        }

		echo $this->template->render($data);

    }

}