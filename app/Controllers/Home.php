<?php
namespace Controllers;
use Resources, Models;

class Home extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();
    }	

    public function index()
    {

		$r['produk']   	= $this->product->readAllProduct($page = 1, $this->limit);

		foreach($r['produk'] as $key => $v) {
			$r['produk'][$key]->gambar = unserialize($v->gambar);
			$r['produk'][$key]->thumbnail = $r['produk'][$key]->gambar[0];
		}

		$data = array(
					'root' => $this->baseConfig['root'],
					'template' => $this->baseConfig['template'],
					'banks' => $this->baseConfig['banks'],
					'categories' => $this->baseConfig['categories'],
					'page' => $this->page->readAllPage('publish'),
					'produk' => $r['produk'],
					'homePage' => true,
					'cartItems' => $this->cart->total_items(),
					'cartTotal' => $this->cart->total(),
					'setting' => $this->setting->read()
				);

		echo $this->template->render($data);
		
    }
}