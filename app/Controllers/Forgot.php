<?php
namespace Controllers;
use Resources, Models;

class Forgot extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();

        $this->kustomer = new \Modules\Admin\Models\Kustomer;
    }

    public function index() {

    	if ($_SERVER['REQUEST_METHOD'] == 'POST') {

    		$_POST['email'] = htmlspecialchars($_POST['email'], ENT_QUOTES);

            // CEK EMAIL YANG SUDAH DAFTAR DI DATABASE
            $cekEmail = $this->kustomer->getIdKustomer($_POST['email']);

            if($cekEmail != NULL) {
	    	
	    		$_POST['password'] = time();

	    		$html = '<h3>Hi,</h3><p>Baru-baru ini kamu lupa password, berikut password yang bisa kamu gunakan</p>
	    				 <p style="font-size: 16px; font-weight: bold;">'.$_POST['password'].'</p>';

	            $this->helper->kirimEmail($_POST['email'], $from = "info@" . str_replace("http://www.", "", $this->uri->baseUri), $subject = "Forgot Password", $text_body = "", $html_body = $html);

	            $this->kustomer->updatePassword($_POST);
            
            } else {
            	echo "<script>alert('Email tidak ditemukan!')</script>";
            }

    	}

        $data['forgotPasswordSuccess'] = true;
		
		$data = array(
		
			'root' => $this->baseConfig['root'],
			'template' => $this->baseConfig['template'],
			'banks' => $this->baseConfig['banks'],
			'categories' => $this->baseConfig['categories'],
			'page' => $this->page->readAllPage('publish'),
			'forgotPassword' => true,
			'cartItems' => $this->cart->total_items(),
			'cartTotal' => $this->cart->total(),
			'setting' => $this->setting->read()
		
		);

		echo $this->template->render($data);

    }

}