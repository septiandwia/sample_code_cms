<?php
namespace Controllers;
use Resources, Models;

class Checkout extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();

        $this->ongkir   = new \Libraries\ongkir;
        $this->kustomer = new \Modules\Admin\Models\Kustomer;
        $this->order    = new \Modules\Admin\Models\Order;

        if(!isset($_SESSION['username'])) {

            // REDIRECT JIKA KERANJANG MASIH KOSONG
            if (!isset($_SESSION['ShoppingCart']) || $_SESSION['ShoppingCart']['total_items'] <= 0 || $_SESSION['ShoppingCart']['total_items'] == 0) {
                echo '<script>alert("Your cart is empty!")</script>';
                echo '<script>window.location.href="'.$this->baseConfig['root'].'"</script>';
            }

        }

    }

    public function index() {

    	$data['checkout'] 	= true;
        $data['categories'] = $this->baseConfig['categories'];
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
        $data['title']      = 'CHECKOUT';
        $data['setting']    = $this->setting->read();
        $data['page']   = $this->page->readAllPage('publish');

   		$contents = $this->cart->contents();

    	$data['cartItems'] = $this->cart->total_items();
    	$data['cartTotal'] = $this->cart->total();
    	$data['ShoppingCart'] = $contents;

    	echo $this->template->render($data);
    	
    }

    public function shipping() {

    	$data['shipping'] = true;
        $data['categories'] = $this->baseConfig['categories'];
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
	    $data['title'] 		= 'Shipping';
        $data['setting']    = $this->setting->read();
    	$data['cartItems']  = $this->cart->total_items();
    	$data['cartTotal']  = $this->cart->total();
        $data['page']   = $this->page->readAllPage('publish');   	

    	echo $this->template->render($data);
    }

    public function login() {

        $data['setting']    = $this->setting->read();
        $data['categories'] = $this->baseConfig['categories'];
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
        $data['page']   = $this->page->readAllPage('publish');
        $data['cartItems'] = $this->cart->total_items();
        $data['cartTotal'] = $this->cart->total();        

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {  

            $_POST['email'] = $this->request->post('email', FILTER_SANITIZE_STRING);
            $_POST['password'] = $this->request->post('password', FILTER_SANITIZE_STRING);
            $_POST['password'] = md5($_POST['password']);

            // CEK EMAIL YANG SUDAH DAFTAR DI DATABASE
            $kustomer = $this->kustomer->loginKustomer($_POST['email'], $_POST['password']);

            if ($kustomer != NULL) {

                $_SESSION['user_email'] = $_POST['email'];
                $data['shippingMember'] = true;                
                $data['kustomerArray']  = unserialize($kustomer->alamat);
                $data['address']        = $data['kustomerArray']['alamat'];
                $data['kustomerArray']  = json_decode($data['kustomerArray']['alamat_ongkir']);
                $data['customer']   = $data['kustomerArray']->rajaongkir->results;
                $data['customer']->id = $kustomer->id_kustomer;
                $data['shipping_address']   = $data['kustomerArray']->rajaongkir->query;

                $data['title']      = 'Shipping Member';
            
            } else {

                $contents = $this->cart->contents();

                $data['checkout']   = true;
                $data['title']      = 'CHECKOUT';
                $data['errorLogin'] = 'email atau password tidak sesuai!';
                $data['ShoppingCart'] = $contents;

            }

            echo $this->template->render($data);            
        
        }

    }

    public function orderMember() {
        
        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            if (isset($_POST['provinceEdit']) || $_POST['provinceEdit'] != 0)
                $_POST['idprovince'] = (int)$_POST['provinceEdit'];

            if (isset($_POST['cityEdit']) || $_POST['cityEdit'] != 0)
                $_POST['idcity'] = (int)$_POST['cityEdit'];
            
            $_POST['alamat'] = htmlspecialchars($_POST['address'], ENT_QUOTES);
            
            $post['alamat_pengiriman']  = 
                serialize(
                    array('alamat' => $_POST['alamat'],
                          'alamat_ongkir' => $this->ongkir->getAddress((int)$_POST['idcity'], (int)$_POST['idprovince']))
                         );
            $post['jenis_pengiriman']   = $_POST['jenisPengirimanMember'];
            $post['id_kustomer']        = (int)$_POST['id'];
            $post['details']            = serialize($_SESSION['ShoppingCart']);
            $post['total_harga']        = (int)$_SESSION['ShoppingCart']['cart_total'];

            // UPDATE DATA MEMBER
            if ($this->kustomer->updatePengiriman($post['id_kustomer'], $post['alamat_pengiriman']))
            {
                $invoice_id = $this->order->insertOrder($post);
                // MEMASUKAN DATA ORDER
                if ($invoice_id != NULL) {
                    $data['invoice_id'] = (isset($invoice_id)) ? '00' . $invoice_id : '0';
                    $data['banks']  = $this->baseConfig['banks'];
                    $data['user_email'] = $_SESSION['user_email']; //MENGAMBIL EMAIL USER
                    $data['created_date'] = date('j F Y');
                    $data['ShoppingCart']    = $this->cart->contents();
                    $data['total_harga']     = $this->cart->total();
                    $data['jenisPengString'] = explode("Rp.", $_POST['jenisPengirimanMember']);
                    $data['jenisPengiriman'] = $data['jenisPengString'][0];
                    $data['hargaPengiriman'] = str_replace(" ", "", str_replace(".", "", $data['jenisPengString'][1]));
                    $data['hargaPengiriman'] = (int)$data['hargaPengiriman'];                
                    // DESTROY SHOPPING CART
                    $this->cart->destroy();
                }
            }

            // PROSES KIRIM EMAIL KE PELANGGAN
            $html = $this->getInvoiceHTML($data);            
            $this->helper->kirimEmail($data['user_email'], $from = "info@" . str_replace("http://www.", "", $this->uri->baseUri), $subject = "Invoice Pemesanan Anda", $text_body = "", $html_body = $html);

            $data['order'] = true;
            $data['title'] = 'Order';
            $data['categories'] = $this->baseConfig['categories'];
            $data['root'] = $this->baseConfig['root'];
            $data['template'] = $this->baseConfig['template'];
            $data['page']   = $this->page->readAllPage('publish');

            echo $this->template->render($data);            
        
        } else {
            echo "<h1>PAGE NOT FOUND!</h1>";
        }
        
    }

    public function order() {

        if ($_SERVER['REQUEST_METHOD'] == 'POST') {

            // CEK EMAIL YANG SUDAH DAFTAR DI DATABASE
            $cekEmail = $this->kustomer->getIdKustomer(htmlspecialchars($_POST['email'], ENT_QUOTES));

            if ($cekEmail == NULL)
            {
                $_POST['nama_lengkap'] = htmlspecialchars($_POST['name'], ENT_QUOTES);
                $_POST['nama_perusahaan'] = htmlspecialchars($_POST['company_name'], ENT_QUOTES);
                $_POST['alamat'] = htmlspecialchars($_POST['address'], ENT_QUOTES);
                $_POST['id_kota'] = (int)$_POST['city'];
                $_POST['id_provinsi'] = (int)$_POST['province'];
                $_POST['alamat'] = serialize(array('alamat' => $_POST['alamat'], 'alamat_ongkir' => $this->ongkir->getAddress($_POST['id_kota'], $_POST['id_provinsi'])));
                $_POST['email'] = htmlspecialchars($_POST['email'], ENT_QUOTES);
                $_SESSION['user_email'] = $_POST['email'];
                $_POST['telpon'] = htmlspecialchars($_POST['phone'], ENT_QUOTES);
                $_POST['password'] = (!empty($_POST['password'])) ? md5($_POST['password']) : NULL;
                $_POST['jenis_pengiriman'] = htmlspecialchars($_POST['jenisPengiriman'], ENT_QUOTES);

                // MEMASUKAN DATA KUSTOMER
                if($this->kustomer->insert($_POST)) {

                    $data['id_kustomer'] = $this->kustomer->getIdKustomer($_POST['email']);

                    if(isset($data['id_kustomer'])) {
                        
                        $post['alamat_pengiriman'] = $_POST['alamat'];
                        $post['nama_lengkap'] = $_POST['nama_lengkap'];
                        $post['jenis_pengiriman'] = $_POST['jenis_pengiriman'];
                        $post['id_kustomer'] = $data['id_kustomer']->id_kustomer;
                        $post['details'] = serialize($_SESSION['ShoppingCart']);
                        $post['total_harga'] = (int)$_SESSION['ShoppingCart']['cart_total'];

                        // MEMASUKAN DATA ORDER
                        $invoice_id = $this->order->insertOrder($post);

                        if($invoice_id != NULL) {

                            $data['invoice_id'] = (isset($invoice_id)) ? '00' . $invoice_id : '0';
                            $data['banks']  = $this->baseConfig['banks'];
                            $data['created_date'] = date("j F Y");
                            $data['user_email'] = $_SESSION['user_email']; //MENGAMBIL EMAIL USER
                            $data['ShoppingCart'] = $this->cart->contents();
                            $data['total_harga'] = $this->cart->total();
                            $data['jenisPengString'] = explode("Rp.", $_POST['jenis_pengiriman']);
                            $data['jenisPengiriman'] = $data['jenisPengString'][0];
                            $data['hargaPengiriman'] = str_replace(" ", "", str_replace(".", "", $data['jenisPengString'][1]));
                            $data['hargaPengiriman'] = (int)$data['hargaPengiriman'];
                            // DESTROY SHOPPING CART
                            $this->cart->destroy();
                        
                        }

                    }
                
                }
                
                $data['status'] = "BERHASIL";
                // PROSES KIRIM EMAIL KE PELANGGAN
                $html = $this->getInvoiceHTML($data);        
                $this->helper->kirimEmail($data['user_email'], $from = "info@" . str_replace("http://www.", "", $this->uri->baseUri), $subject = "Invoice Pemesanan Anda", $text_body = "", $html_body = $html);                 

            } else {
            
                $data['emailExist'] = true;

            }
            
        }

        $data['order'] = true;
        $data['categories'] = $this->baseConfig['categories'];
        $data['root']       = $this->baseConfig['root'];
        $data['template']       = $this->baseConfig['template'];
        $data['page']   = $this->page->readAllPage('publish');

        echo $this->template->render($data);

    }

    public function getProvince() {
    	
    	$data = $this->ongkir->getProvince();

		header('Content-Type: application/json');
		echo $data;

    }

    public function getCity() {

        $idProvince = (int)$_POST['id_province'];
    	
    	$data = $this->ongkir->getCity($idProvince);

		header('Content-Type: application/json');
		echo $data;

    }

    public function getCost() {

    	$this->setting  = new \Modules\Admin\Models\Setting;

        $origin = $this->setting->selectOrigin();
        $origin = $origin[0]->id_city;

    	$data = $this->ongkir->getCost($origin, $_POST['idcity'], $this->cart->total_weight(), $_POST['kurir']);

		header('Content-Type: application/json');
		echo $data;
		
    }

    public function getInvoiceHTML($data = array()) {

        $html = '<!DOCTYPE html>
<html>
<head>
    <title></title>
    <style>
.invoice-box {
    background-color: #fff;
    max-width: 800px;
    margin: auto;
    margin-bottom: 3em;
    padding: 30px;
    border: 1px solid #eee;
    box-shadow: 0 0 10px rgba(0, 0, 0, .15);
    font-size: 16px;
    line-height: 24px;
    font-family: "Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
    color: #555;
}

.invoice-box table {
    width: 100%;
    line-height: inherit;
    text-align: left;
}

.invoice-box table td {
    padding: 5px;
    vertical-align: top;
}

.invoice-box table tr td:nth-child(2) {
    text-align: right;
}

.invoice-box table tr.top table td {
    padding-bottom: 20px;
}

.invoice-box table tr.top table td.title {
    font-size: 45px;
    line-height: 45px;
    color: #333;
}

.invoice-box table tr.information table td {
    padding-bottom: 40px;
}

.invoice-box table tr.heading td {
    background: #eee;
    border-bottom: 1px solid #ddd;
    font-weight: bold;
}

.invoice-box table tr.details td {
    padding-bottom: 20px;
}

.invoice-box table tr.item td {
    border-bottom: 1px solid #eee;
}

.invoice-box table tr.item.last td {
    border-bottom: none;
}

.invoice-box table tr.total td:nth-child(2) {
    border-top: 2px solid #eee;
    font-weight: bold;
}
    </style>
</head>
<body>';
        $html .= '<div class="invoice-box">
            <table cellpadding="0" cellspacing="0">
                <tr class="top">
                    <td>
                        <table>
                            <tr>
                                <td>
                                    Invoice #: '.$data['invoice_id'].'<br>
                                    Created: '.$data['created_date'].'<br>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                
                <tr class="heading">
                    <td>Item</td>
                    <td>Price</td>
                </tr>';

                foreach($data['ShoppingCart'] as $key => $val) {
                    $html .= '<tr class="item">
                                    <td>'.$val['name'].', quantity: '.$val['quantity'].'</td>
                                    <td>Rp. '.number_format($val['price'] * $val['quantity'],0,',','.').'</td>
                                </tr>'; 
                }

                $html .= '
                  <tr class="item">
                    <td>'.$data['jenisPengiriman'].'</td>
                    <td>Rp. '.number_format($data['hargaPengiriman'],0,',','.').'</td>
                  </tr>
                
                <tr class="total">
                    <td></td>
                    
                    <td>
                       Total: Rp. '.number_format($data['total_harga'] + $data['hargaPengiriman'],0,',','.').',-
                    </td>
                </tr>
                <tr class="item">
                  <td colspan="2">
                    Ini adalah invoice dan daftar pesanan anda. Silahkan konfirmasi pesanan anda jika anda sudah melakukan pembayaran.
                  </td>
                </tr>
                <tr class="item">
                  <td colspan="2">
                    Pembayaran Melalui transfer bank yang tertera dibawah:
                  </td>
                </tr>
                <tr class="item">
                  <td colspan="2">';
                  foreach($this->baseConfig['banks'] as $val){
                    $html .= '<p><strong>'.$val->nama_bank.'</strong><br /><strong>No. Rek: '.$val->no_rekening.'</strong><br /><strong>a.n '.$val->pemilik.'</strong></p>';
                  }
                  $html .= '</td>
                </tr>                
            </table>
        </div> ';

        $html .= '</body></html>';
        return $html;

    }

}