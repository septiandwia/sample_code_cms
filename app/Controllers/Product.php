<?php
namespace Controllers;
use Resources, Models;

class Product extends Resources\ControllerFront
{    

    public function __construct(){
        parent::__construct();
    }

    public function index($page = 1) {

        $this->pagination = new Resources\Pagination();  

        $data = array(
                    'cartItems' => $this->cart->total_items(),
                    'cartTotal' => $this->cart->total()                
                );        

        $page  = (isset($_GET['page'])) ? (int)$_GET['page'] : (int)$page;
        $limit = $this->limit;

        if(isset($_GET['search'])){

            $search = stripslashes(strip_tags(htmlspecialchars($_GET['search'],ENT_QUOTES)));

            $data['products']   = $this->product->searchProduct($page, $limit, $search);
            $data['title'] = 'Search Result: ' . $search;

        } else {

            $data['products']   = $this->product->readAllProduct($page, $limit, 'id_produk, nama_produk, nama_kategori, gambar, harga, produk.slug');
            $data['title'] = 'All Product';

        }

        foreach($data['products'] as $key => $v) {
            $data['products'][$key]->gambar = unserialize($v->gambar);
            $data['products'][$key]->thumbnail = $data['products'][$key]->gambar[0];
        }

        $total = $this->product->myTotalProduct();
        
        if($total > $limit) {

            $data['pageLinks'] = $this->pagination
                ->setOption(
                    array(
                        'limit'     => $limit,
                        'base'      => $this->baseConfig['root'].'product?page=%#%',
                        'total'     => $total,
                        'current'   => $page
                    )
                )
                ->getUrl();

        }

        $data['listProduk'] = true;
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
        $data['categories'] = $this->baseConfig['categories'];
        $data['setting']    = $this->setting->read();
        $data['page']    = $this->page->readAllPage('publish');

        echo $this->template->render($data);

    }

    public function alias($name) {

        $name = stripslashes(strip_tags(htmlspecialchars($name,ENT_QUOTES)));
    	// $id = (int)$id;

        $data['categories'] = $this->baseConfig['categories'];
        $data['setting']    = $this->setting->read();
        $data['page']    = $this->page->readAllPage('publish');
        $data['cartItems']  = $this->cart->total_items();
        $data['cartTotal']  = $this->cart->total();
    	$data['detailProduk'] = true;
        $data['root'] = $this->baseConfig['root'];
        $data['template'] = $this->baseConfig['template'];
	    $data['product'] 	= $this->product->readBySlug($name);
	    $data['product'] 	= $data['product'][0];
	    $data['title'] = $data['product']->nama_produk;
	    $data['product']->gambar = unserialize($data['product']->gambar);
	    $data['product']->deskripsi = html_entity_decode($data['product']->deskripsi);
	    $data['product']->productURL = $data['root'] . "product/" . $data['product']->slug;

		echo $this->template->render($data);	   	
    }

}