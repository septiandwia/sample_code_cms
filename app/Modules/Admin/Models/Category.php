<?php
namespace Modules\Admin\Models;
use Resources, Modules\Admin;

class Category {

	public function __construct() {
		$this->db = new Resources\Database;
	}

	public function readAllCategory() {
		
		$sql  = "SELECT kategori.id_kategori, kategori.nama_kategori, kategori.parent_id, kategori.slug FROM kategori ORDER BY id_kategori DESC";

		$data =  $this->db->results($sql);

		if($data == false) return false;

		foreach($data as $key => $v) {
			$row[$v->parent_id][$key] = $v;
			$row[$v->parent_id][$key]->jmlProduk = $this->countProduct($v->id_kategori);
		}

		return $row;
	
	}

	public function readAllCategoryFront() {
		
		$sql  = "SELECT * FROM kategori ORDER BY id_kategori DESC";

		$data =  $this->db->results($sql);

		if($data == false) return false;

		foreach($data as $key => $v) {
			$row[$v->parent_id][] = $v;
		}

		return $row;		
	
	}

	public function countProduct($id) {
		$result = $this->db->results("SELECT COUNT(id_produk) AS jmlProduk FROM produk WHERE id_kategori = '".$id."'");
		return $result[0]->jmlProduk;
	}

	public function inputCategory($data) {

		$result = $this->db->query("INSERT INTO kategori (nama_kategori, parent_id, slug) VALUES ('".$data['name']."', '".$data['kategori']."', '".$data['kategori_seo']."')");
		
		if($result) return $data;
	
	}

	public function deleteCategory($id) {
		
		$result = $this->db->delete('kategori', array('id_kategori' => $id));
		
		if($result) return true;
	
	}

	public function updateCategory($id, $data) {
		$result = $this->db->update('kategori',
								array(
									'nama_kategori' => $data['name'],
									'parent_id' => $data['kategori'],
									'slug' => $data['kategori_seo']
									),
								array('id_kategori' => $id)
							);
		if($result) return $data;
	}

	public function readCategoryById($id) {

		$data = $this->db->results("SELECT nama_kategori, id_kategori FROM kategori WHERE id_kategori = '".(int)$id."'");

		return $data[0];

	}

	public function cekId($name) {
		$data = $this->db->results("SELECT id_kategori FROM kategori WHERE slug = '".$name."'");
		return $data[0]->id_kategori;
	}

	public function slug_exist($slug) {
		$query = $this->db->query("SELECT id_kategori FROM kategori WHERE slug = '".$slug."'");
		return $query->num_rows;
	}

}