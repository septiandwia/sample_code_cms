<?php
namespace Modules\Admin\Models;
use Resources, Modules\Admin;

class Product {

	public function __construct() {
		$this->db = new Resources\Database;
	}

	public function readAllProduct($page = 1, $limit = 10, $field = "", $id_kategori = "") {

		if($field == "") $field = "*";

		if($id_kategori != "") $id_kategori = "AND produk.id_kategori = '".$id_kategori."'";

		$offset = ($limit * $page) - $limit;
		$result = $this->db->results("SELECT ".$field.", produk.slug AS slug FROM produk, kategori WHERE produk.id_kategori = kategori.id_kategori ".$id_kategori." ORDER BY id_produk DESC LIMIT $offset, $limit");
		
		return $result;

	}

	public function searchProduct($page = 1, $limit = 10, $search) {

		$search = "LIKE '%".$search."%'";
		$offset = ($limit * $page) - $limit;

		$result = $this->db->results("SELECT produk.id_produk, produk.nama_produk, produk.slug, produk.harga, produk.harga, produk.gambar, kategori.nama_kategori FROM produk, kategori WHERE produk.id_kategori = kategori.id_kategori AND produk.nama_produk ".$search." ORDER BY id_produk DESC LIMIT $offset, $limit");

		return $result;		

	}

	public function readProduct($id, $field = "") {
		if($field == "") $field = "*";

		$result = $this->db->results("SELECT ".$field." FROM produk WHERE id_produk = '".$id."'");
		return $result;
	}

	public function readBySlug($name) {
		$result = $this->db->results("SELECT * FROM produk WHERE slug = '".$name."'");
		return $result;		
	}

	public function myTotalProduct() {
		return $this->db->getVar("SELECT COUNT(id_produk) FROM produk");
	}

	public function myTotalProductCategory($id) {
		return $this->db->getVar("SELECT COUNT(id_produk) FROM produk WHERE id_kategori = '".$id."'");	
	}

	public function inputProduct($data) {

		$sql = "INSERT INTO produk (id_kategori, nama_produk, deskripsi, harga, stok, berat, sku, gambar, publish, slug) VALUES ('".$data['kategori']."', '".$data['prod-name']."', '".$data['deskripsi']."', '".$data['harga']."', '".$data['quantity']."', '".$data['berat']."', '".$data['sku']."', '".$data['foto']."', '".$data['publish']."', '".$data['produk_seo']."')";
		
		$result = $this->db->query($sql);
		
		if($result) return $data;
	}

	public function updateProduct($data) {

		$dataSave = array(

					"id_kategori" 	=> $data['kategori'],
					"nama_produk" 	=> $data['prod-name'],
					"slug"			=> strtolower(str_replace(" ", "-", $data['prod-name'])),
					"deskripsi"		=> $data['deskripsi'],
					"harga"			=> $data['harga'],
					"stok"			=> $data['quantity'],
					"berat"			=> $data['berat'],
					"publish"		=> $data['publish'],
					"sku"		=> $data['sku'],
					"gambar"		=> (isset($data['foto'])) ? $data['foto'] : ''
				
				);

		$query = $this->db->update('produk',
									$dataSave,
									array('id_produk' => $data['id_produk'])
								);

		if($query) return $data;
	
	}

	public function deleteProduct($id) {
		
		// HAPUS GAMBAR PRODUK
		$query = $this->db->select('gambar')->from('produk')->getAll();
		$query['foto'] = unserialize($query[0]->gambar);

		foreach($query['foto'] as $v) {
			$foto = explode('upload', $v);
			unlink('./upload/'.$foto[1]);
		}

		// HAPUS DATA DI DATABASE
		$query = $this->db->delete('produk', array('id_produk' => $id)); 
		return $query;
	}

	public function readProductByIdCategory($id) {
		$result = $this->db->results("SELECT * FROM produk WHERE id_kategori = '".$id."'");
		return $result;		
	}

	public function slug_exist($slug) {
		$query = $this->db->query("SELECT id_produk FROM produk WHERE slug = '".$slug."'");
		return $query->num_rows;
	}

}