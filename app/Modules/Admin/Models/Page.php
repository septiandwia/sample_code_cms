<?php
namespace Modules\Admin\Models;
use Resources, Modules\Admin;

class Page {

	public function __construct() {
		$this->db = new Resources\Database;
	}

	public function addPage($data) {

		$query = $this->db->insert('berita', 
								array(
									'username' 	=> $data['username'],
									'title'		=> $data['title'],
									'slug'		=> $data['judul_seo'],
									'content'	=> $data['content'],
									'publish'	=> $data['isPublished']
								)
							);
		return $query;

	}

	public function readPage($id) {
		$result = $this->db->results("SELECT * FROM berita WHERE id_berita = '".$id."'");
		return $result;
	}

	public function readPageSlug($name) {
		$result = $this->db->results("SELECT * FROM berita WHERE slug = '".$name."'");
		return $result;
	}

	public function readAllPage($status = "") {
		if($status != "") $status = "WHERE publish = '1'";

		$result = $this->db->results("SELECT title, slug, publish, tanggal, id_berita FROM berita ".$status." ORDER BY id_berita DESC");
		return $result;		
	}

	public function deletePage($id) {
		$query = $this->db->delete('berita', array('id_berita' => $id)); 
		return $query;		
	}

	public function updatePage($data) {
		
		$query = $this->db->update('berita', 
								array(
									'username' 	=> $data['username'],
									'title'		=> $data['title'],
									'slug'		=> $data['judul_seo'],
									'content'	=> $data['content'],
									'publish'	=> $data['isPublished']
								),
								array('id_berita' => $data['id_berita'])
							);
		return $query;
	
	}

	public function updatePublish($id, $data) {
		$query = $this->db->update('berita', array('publish' => $data['publish']), array('id_berita' => $id));
		return $query;
	}

}