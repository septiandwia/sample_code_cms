<?php
namespace Modules\Admin\Models;
use Resources, Modules\Admin;

class Order {

	public function __construct() {
		$this->db = new Resources\Database;
	}

	public function readAllOrder($page = 1, $limit = 10) {

		$offset = ($limit * $page) - $limit;
		$result = $this->db->results("SELECT orders.id_orders, orders.id_kustomer, orders.status_order, kustomer.nama_lengkap, kustomer.id_kustomer, kustomer.telpon, kustomer.email FROM orders,kustomer WHERE orders.id_kustomer=kustomer.id_kustomer ORDER BY id_orders DESC LIMIT $offset, $limit");

		return $result;
	
	}

	public function getTotalSalesMonth() {
		
		$start = date("Y-m") . '-01';
		$end   = date("Y") . "-" . date("m", strtotime("+1 month")) . "-01";
		
		$sql = "SELECT SUM(total_harga) AS totalHarga FROM orders 
				WHERE tgl_order >= '".$start."'
				AND tgl_order < '".$end."'";
 		$sql = $this->db->results($sql);

 		return $sql[0]->totalHarga;
	}

	public function getTotalPerDay() {
		
		$start = date("Y-m") . '-01';
		$end   = date("Y") . "-" . date("m", strtotime("+1 month")) . "-01";
		
		$sql = "SELECT COUNT(tgl_order) AS jumlah, tgl_order FROM orders 
				WHERE tgl_order >= '".$start."'
				AND tgl_order < '".$end."' GROUP BY DATE(tgl_order)";
 		$sql = $this->db->results($sql);

 		return $sql;
	
	}

	public function latestOrder($limit = 5) {
		$result = $this->db->results("SELECT orders.id_orders, orders.id_kustomer, orders.status_order, kustomer.nama_lengkap, kustomer.id_kustomer, kustomer.telpon, kustomer.email FROM orders,kustomer WHERE orders.id_kustomer=kustomer.id_kustomer ORDER BY id_orders DESC LIMIT 0, $limit");
		return $result;
	}

	public function myTotalOrder() {
		return $this->db->getVar("SELECT COUNT(id_orders) FROM orders");
	}

	public function myTotalHarga() {
		$result = $this->db->results("SELECT SUM(total_harga) AS total FROM orders");
		return $result[0]->total;
	}

	public function detailOrder($id) {

		$result = $this->db->results("SELECT * FROM orders WHERE id_orders = '".$id."'");

		return $result;
	
	}

	public function insertOrder($data) {

		$sql = "INSERT INTO orders (id_kustomer, details, total_harga, status_order, jenis_pengiriman, alamat_pengiriman) VALUES ('".$data['id_kustomer']."', '".$data['details']."', '".$data['total_harga']."', 'baru', '".$data['jenis_pengiriman']."', '".$data['alamat_pengiriman']."')";
		$result = $this->db->query($sql);
		$last_id = $this->db->insertId($sql);
		
		return $last_id;
	}

	public function updateStatus($id, $status_order) {

		$result = $this->db->update('orders', array('status_order' => $status_order), array('id_orders' => $id));

		return $result;

	}

	public function removeQuantity($data) {
		
		$result = $this->db->query("UPDATE produk SET stok = stok - " .$data['quantity'] . " WHERE id_produk = '".$data['id']."'");
		
		// UPDATE DIBELI
		if($result) {
			$this->addDibeli($data);
		}
		return $result;
	
	}

	public function addDibeli($data) {
		$result = $this->db->query("UPDATE produk SET dibeli = dibeli + " .$data['quantity'] . " WHERE id_produk = '".$data['id']."'");
		return $result;
	}

	public function getQuantity($id) {

		$data = array();		
		$results = $this->db->results("SELECT details FROM orders WHERE id_orders = '".$id."'");
		$results = unserialize($results[0]->details);

		unset($results['cart_total'], $results['total_items']);
		
		$i = 0;
		foreach($results as $key => $val) {
			$data[$i]['id'] = $val['id'];
			$data[$i]['quantity'] = $val['quantity'];
			$i++;
		}
		
		return $data;
	}

}