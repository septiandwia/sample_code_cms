<?php
namespace Modules\Admin\Models;
use Resources, Modules\Admin;

class Setting {

	public function __construct() {
		$this->db = new Resources\Database;
	}

	public function update($data) {

		$query = $this->db->update(
							'setting',
							array(
								'title' => $data['title'],
								'description' => $data['description']
							),
							array(
								'id_setting' => $data['id_setting']
							)
						);
		return $query;

	}

	public function read() {
		$result = $this->db->results("SELECT * FROM setting WHERE id_setting = '1'");
		return $result[0];
	}

	public function updateCity($cityName, $cityId) {

		$result = $this->db->update('setting', array('city' => $cityName, 'id_city' => $cityId), array('id_setting' => '1'));

		return $result;
	
	}

	public function getCityName() {
		$result = $this->db->results("SELECT city FROM setting WHERE id_setting = '1'");
		return $result;
	}

	public function selectOrigin() {
		$result = $this->db->results("SELECT id_city FROM setting WHERE id_setting = '1'");
		return $result;
	}

	public function updatePassword($data) {
		$result = $this->db->update('admins', array('password' => $data), array('username' => 'admin'));
		return $result;
	}


}