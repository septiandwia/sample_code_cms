<?php
namespace Modules\Admin\Models;
use Resources, Modules\Admin;

class Payment {

	public function __construct() {
		$this->db = new Resources\Database;
	}

	public function addPayment($data) {
		
		$sql = "INSERT INTO mod_bank (nama_bank, no_rekening, pemilik, gambar) VALUES ('".$data['namaBank']."', '".$data['noRekening']."', '".$data['atasNama']."', '".$data['fileName']."')";
		$sql = $this->db->query($sql);

		return $sql;
	
	}

	public function readPayment() {

		$sql = $this->db->select()->from('mod_bank')->getAll();
		return $sql; 
	
	}

	public function deletePayment($id) {
	
		$sql = $this->db->delete('mod_bank', array('id_bank' => $id));
		return $sql;
	
	}

}