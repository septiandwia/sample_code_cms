<!DOCTYPE html>
<html>

<head>
    <meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Online Shop Panel Management</title>
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/bootstrap-2.css">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/bootstrap-2.responsive.min.css">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/style.css">

    <style type="text/css">
      body {
        padding-top: 40px;
        padding-bottom: 40px;
        background-color: #eee;
      }

      .form-signin {
        max-width: 330px;
        padding: 15px;
        margin: 0 auto;
      }
      .form-signin .form-signin-heading,
      .form-signin .checkbox {
        margin-bottom: 10px;
      }
      .form-signin .checkbox {
        font-weight: normal;
      }
      .form-signin .form-control {
        position: relative;
        height: auto;
        -webkit-box-sizing: border-box;
           -moz-box-sizing: border-box;
                box-sizing: border-box;
        padding: 10px;
        font-size: 16px;
      }
      .form-signin .form-control:focus {
        z-index: 2;
      }
      .form-signin input[type="email"] {
        margin-bottom: -1px;
        border-bottom-right-radius: 0;
        border-bottom-left-radius: 0;
      }
      .form-signin input[type="password"] {
        margin-bottom: 10px;
        border-top-left-radius: 0;
        border-top-right-radius: 0;
      }
    </style>
</head>

<body OnLoad="document.login.username.focus();">
    <div id="login" class="container">

      <form method="POST" action="<?php echo $this->location('admin/auth/login'); ?>" class="form-signin">
        <h2 class="form-signin-heading">Please sign in</h2>
        <label for="inputEmail" class="sr-only">username</label>
        <input style="width: 100%;" name="username" type="text" id="username" class="form-control" placeholder="Masukan username" required autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input style="width: 100%;" name="password" type="password" id="inputPassword" class="form-control" placeholder="Masukan Password" required>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Sign in</button>
      </form>

    </div> <!-- /container -->

</body>

</html>
