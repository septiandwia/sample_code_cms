<script type="text/javascript">
	
	$(document).ready(function() {
		
		$('#save').on('click', function() {

			var dataSetting = $("#formSetting").serializeArray();
			
			$.post('<?=$this->location('admin/setting/update')?>', dataSetting, function(data, textStatus, xhr) {

            if(textStatus == 'success') {
               $('#alert').empty();
               $('#alert').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info</strong> Data Berhasil di Save!</div>');
               $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                  $(".alert").slideUp(500);
               });
            }

			});

		});

	});

</script>

<h4>Site settings</h4>
<hr>
<div id="alert"></div>
<div class="row-fluid">
   <form id="formSetting" class="form-horizontal">
      <input value="1" name="id_setting" type="hidden" />
      <div class="control-group">
         <label class="control-label" for="title">Title:</label>
         <div class="controls">
            <input class="span6" name="title" id="title" placeholder="Site title ..." value="<?=(isset($setting)) ? $setting->title : '' ?>" type="text">
         </div>
      </div>
      <div class="control-group">
         <label class="control-label" for="description">Description:</label>
         <div class="controls">
            <textarea class="span6" rows="6" id="description" name="description"><?=(isset($setting)) ? $setting->description : '' ?></textarea>
            <span class="help-block">Deskripsikan website anda</span>
         </div>
      </div>
   </form>
   <div class="form-horizontal">
      <div class="control-group">
         <div class="controls">
            <button id="save" class="save-domain btn">Save</button>
         </div>
      </div>   
    </div>
</div>

