<h3>Cara mengedit halaman website anda</h3>
<hr>
<h4>Pengelanan</h4>
<p>Untuk dapat mengedit halaman website anda, dibutuhkan beberapa keterampilan dasar menggunakan HTML. </p>
<br>
<h4>Operators</h4>
<p>Kami menyediakan dua jenis operator untuk dapat digunakan dalam membangun sebuah tema dan menyisipkannya pada kode HTML anda, yang pertama adalah variable dengan delimiters: {{ ... }} dan blocks dengan delimeter: {% ... %}.</p>
<p>Variable digunakan untuk memasukan data dinamis seperti judul website atau deskripsi website anda:</p>
<h5>Contoh</h5>
<pre>&lt;html&gt;
    &lt;head&gt;
        &lt;title&gt;{{ root }}&lt;/title&gt;
    &lt;/head&gt;
    &lt;body&gt;
        ...
    &lt;/body&gt;
&lt;/html&gt;</pre>
<p>Beberapa variable mungkin memiliki elemen atribut didalamnya atau varibale yang anda bisa mengaksi elemen didalamnya. Anda dapat menggunakan tanda "titik" (.) untuk dapat mengakses atribut di dalam variable.</p>
<pre>{{ setting.title }}</pre>
<pre>{{ setting.description }}</pre>
<p>Blok juga dapat digunakan untuk me-render sebuah blok HTML yang berisi satu set data atau biasanya untuk me-render sebuah blok HTML:</p>
<pre>&lt;html&gt;
    &lt;body&gt;
    {% if produk %}
       &lt;ul class="thumbnails listing-products"&gt;
        {% for prod in produk %}
            &lt;li class="span3"&gt;
                &lt;a href="{{ product.name }}" class="title"&gt;{{ product.title }}&lt;/a&gt;
            &lt;/li&gt;
        {% endfor %}
       &lt;/ul&gt;
    {% endif %}
    &lt;/body&gt;
&lt;/html&gt;</pre>
<br>
<h4>Struktur dasar HTML</h4>
<p>Setiap Template HTML harus terdiri dari beberapa blok yang merepresentasikan setiap halaman website.</p>
<pre>&lt;html&gt;
    &lt;body&gt;
    {% if homePage %}
      <!-- HALAMAN UTAMA -->
    {% endif %}

    {% if staticPage %}
      <!-- HALAMAN PAGE / STATIS PAGE -->
    {% endif %}

    {% if detailProduk %}
      <!-- HALAMAN DETAIL PRODUK -->
    {% endif %}

    {% if listProduk %}
      <!-- HALAMAN PRODUK PER KATEGORI -->
    {% endif %}

    {% if checkout %}
      <!-- HALAMAN CHECKOUT -->
    {% endif %}

    {% if shippingMember %}
      <!-- HALAMAN SHIPPING MEMBER -->
    {% endif %}

    {% if shipping %}
      <!-- HALAMAN SHIPPING BUKAN MEMBER -->
    {% endif %}

    {% if order %}
      <!-- HALAMAN ORDER / INVOICE -->
    {% endif %}

    {% if forgotPassword %}
      <!-- HALAMAN FORGOT PASSWORD -->
    {% endif %}

    {% if detailCart %}
      <!-- HALAMAN LIST KERANJANG / CART -->
      
      {% if emptyCart %}
      <!-- HALAMAN JIKA KERANJANG KOSONG -->
      {% else %}

    {% endif %}

    {% if pageLinks %}
      <!-- LINK UNTUK PAGINATION PAGE -->
    {% endif %}
    &lt;/body&gt;
&lt;/html&gt;</pre>
<p>Here's an example of the complete markup for a Template:</p>
<pre></pre>
<br>
<h4>Variables and Blocks</h4>
<table class="table table-striped">
   <thead>
      <tr>
         <th>Variable and block</th>
         <th>Description</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>{{ setting.title }}</td>
         <td>Menampilkan judul website anda.</td>
      </tr>
      <tr>
         <td>{{ setting.description }}</td>
         <td>Menampilkan deskripsi website anda.</td>
      </tr>
      <tr>
         <td>{{ root }}</td>
         <td>Menampilkan alamat website anda, contoh: http://www.namadomain.com/</td>
      </tr>
      <tr>
         <td>{% for category in categories %} {% endfor %}</td>
         <td>List blok menampilkan kategori yang ada.</td>
      </tr>
      <tr>
         <td>{{ category.slug }}</td>
         <td>Menampilkan URL kategori SLUG.</td>
      </tr>
      <tr>
         <td>{{ category.nama_kategori }}</td>
         <td>Menampilkan nama kategori.</td>
      </tr>
      <tr>
         <td>{% for child in category.childs %} {% endfor %}</td>
         <td>Blok list untuk menampilkan turunan dari kategori parent *jika ada.</td>
      </tr>
      <tr>
         <td>{{ child.slug }}</td>
         <td>Menampilkan url slug kategori turunan/anak kategori.</td>
      </tr>
      <tr>
         <td>{{ child.nama_kategori }}</td>
         <td>Menampilkan nama anak kategori.</td>
      </tr>
      <tr>
         <td>{{ cartItems }}</td>
         <td>Menampilkan jumlah belanjaan dalam keranjang (cart).</td>
      </tr>
      <tr>
         <td>{{ cartTotal }}</td>
         <td>Menampilkan jumlah total belanjaan dalam rupiah.</td>
      </tr>
      <tr>
         <td>{{ title }}</td>
         <td>Menampilkan judul dari setiap halaman.</td>
      </tr>
      <tr>
         <td>{% for prod in produk %} {% endfor %}</td>
         <td>Block list produk.</td>
      </tr>
      <tr>
         <td>{{ prod.slug }}</td>
         <td>Menampilkan nama slug dari produk</td>
      </tr>
      <tr>
         <td>{{ prod.thumbnail }}</td>
         <td>Menampilkan gambar dari setiap produk</td>
      </tr>
      <tr>
         <td>{{ prod.harga }}</td>
         <td>Menampilkan harga produk</td>
      </tr>      
      <tr>
         <td>{{root}}product/{{ prod.slug }}</td>
         <td>Menampilkan link produk. Pada dasarnya setiap link berisi {{ root }} dan dilanjutkan dengan nama slug dari setiap halamannya</td>
      </tr>
      <tr>
         <td>{{ product.title }}</td>
         <td>HTML safe product title.</td>
      </tr>
      <tr>
         <td>{{ staticPageTitle }}</td>
         <td>Menampilkan judul halaman statis / page.</td>
      </tr>
      <tr>
         <td>{{ staticPageContent|raw }}</td>
         <td>Menampilkan konten halaman statis. Perlu diketahui bahwa setiap konten halaman berisi tag HTML yang mana akan kita render dengan menggunakan parameter pipeline (|) raw</td>
      </tr>      
      <tr>
         <td>{{ product.gambar.0 }}</td>
         <td>Menampilkan link gambar detail *jika ada.</td>
      </tr>
      <tr>
         <td>{% for img in product.gambar %}</td>
         <td>Menampilkan gambar pendukung produk</td>
      </tr>
      <tr>
         <td>{{ img }}</td>
         <td>Menampilkan link gambar produk pendukung</td>
      </tr>
      <tr>
         <td>{{ product.deskripsi|raw }}</td>
         <td>Menampilkan deskripsi produk.</td>
      </tr>
      <tr>
         <td>{{ product.harga|number_format(0, ',', '.') }}</td>
         <td>Menampilkan harga produk dengan format rupiah</td>
      </tr>
      <tr>
         <td>{{ product.id_produk }}</td>
         <td>Menampilkan id unik setiap produk</td>
      </tr>
      <tr>
         <td>{{ product.stok }}</td>
         <td>Menampilkan jumlah stok produk yang ada</td>
      </tr>
      <tr>
         <td>{{ product.berat }}</td>
         <td>Menampilkan berat dari setiap produk yang ada.</td>
      </tr>
      <tr>
         <td>{% for cart in ShoppingCart %}</td>
         <td>Menampilkan blok keranjang belanja *cart.</td>
      </tr>
      <tr>
         <td>{{ cart.name }}</td>
         <td>Menampilkan nama produk yang sedang dibeli / berada di keranjang.</td>
      </tr>
      <tr>
         <td>{{ cart.quantity }}</td>
         <td>Menampilkan jumlah barang yang sedang dibeli / berada di keranjang.</td>
      </tr>
      <tr>
         <td>{{ cart.price|number_format(0, ',', '.') }}}</td>
         <td>Menampilkan jumlah harga belanjaan yang sedang dibeli/berada di keranjang dalam format rupiah.</td>
      </tr>
      <tr>
         <td>{{ cart.subtotal|number_format(0, ',', '.') }}</td>
         <td>Menampilkan subtotal dari belanjaan.</td>
      </tr>
      <tr>
         <td>{{ cartTotal|number_format(0, ',', '.') }}</td>
         <td>Menampilkan total belanjaan dalam format rupiah.</td>
      </tr>
      <tr>
         <td>{{ customer.id }}</td>
         <td>Menampilkan id kustomer unik.</td>
      </tr>
      <tr>
         <td>{{ customer.city_id }}</td>
         <td>Menampilkan id kota kustomer.</td>
      </tr>
      <tr>
         <td>{{ customer.province_id }}</td>
         <td>Menampilkan id provinsi kustomer.</td>
      </tr>
      <tr>
         <td>{{ customer.province }}</td>
         <td>Menampilkan nama provinsi kustomer.</td>
      </tr>
      <tr>
         <td>{{ customer.type }}</td>
         <td>Menampilkan letak distrik kostumer (kabupaten/kota) kustomer.</td>
      </tr>
      <tr>
         <td>{{ customer.city_name }}</td>
         <td>Menampilkan nama kota kustomer.</td>
      </tr>
      <tr>
         <td>{{ customer.postal_code }}</td>
         <td>Menampilkan kode pos kustomer.</td>
      </tr>
      <tr>
         <td>{{ invoice_id }}</td>
         <td>Menampilkan id invoice unik dari setiap pemesanan.</td>
      </tr>
      <tr>
         <td>{% for page in pageLinks %} {% endfor %}</td>
         <td>Menampilkan blok link halaman.</td>
      </tr>
      <tr>
         <td>{{ page|raw }}</td>
         <td>Menampilkan link halaman.</td>
      </tr>
      <tr>
         <td>{{ page.title }}</td>
         <td>Menampilkan judul halaman.</td>
      </tr>
      <tr>
         <td>{% if emailExist %}{% endif %}</td>
         <td>Untuk mengecek apakah email sudah terdaftar atau belum</td>
      </tr>
      <tr>
         <td>{% if forgotPassword %}{% endif %}</td>
         <td>Untuk mengecek apakah berada di halaman forgotPassword</td>
      </tr>
      <tr>
         <td>{% if forgotPasswordSuccess %}{% endif %}</td>
         <td>Untuk mengecek apakah proses kirim password ke email berhasil</td>
      </tr>      
   </tbody>
</table>
<br>
<h4>Filters</h4>
<p> Setiap variable dapat dimodifikasi menggunakan filter. Filter selalu dipisahkan dengan menggunakan simbol pipeline (|) dan diikuti dengan beberapa arguments dalam parameternya.</p>
<table class="table table-striped">
   <thead>
      <tr>
         <th>Filters</th>
         <th>Description</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td>|raw</td>
         <td>Berguna untuk menampilkan konten yang berisikan HTML di dalamnya. Seperti deskripsi halaman.</td>
      </tr>
      <tr>
         <td>|number_format(0, ',', '.')</td>
         <td>Berguna untuk memanipulasi setiap angka atau harga dan menampilkannya dalam format rupiah.</td>
      </tr>
   </tbody>
</table>
<br>
<h4>Static Assets</h4>
<p>Jika anda membutuhkan libary-library yang secara umum digunakan dalam pengembangan website seperti jQuery, Bootstrap, Font Awesome dan yang lainnya. Anda dapat secara langsung memasukannya dalam editor. Berikut beberapa CDN yang telah disediakan:</p>
<table class="table table-striped">
   <thead>
      <tr>
         <th>CDN Provider</th>
         <th>URL Example</th>
      </tr>
   </thead>
   <tbody>
      <tr>
         <td><a href="http://www.jsdelivr.com/">jsDelivr</a></td>
         <td>//cdn.jsdelivr.net/jquery/2.1.1/jquery.min.js</td>
      </tr>
      <tr>
         <td><a href="https://cdnjs.com/">cdnjs</a></td>
         <td>//cdnjs.cloudflare.com/ajax/libs/jquery/2.1.1/jquery.min.js</td>
      </tr>
      <tr>
         <td><a href="http://osscdn.com/">OSS MaxCDN</a></td>
         <td>//oss.maxcdn.com/jquery/2.1.1/jquery.min.js</td>
      </tr>
   </tbody>
</table>