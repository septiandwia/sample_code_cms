<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta content="text/html;charset=utf-8" http-equiv="Content-Type">
    <meta content="utf-8" http-equiv="encoding">    
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Online Shop Themes Editor</title>
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/bootstrap-2.css">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/style.css">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/featherlight.css">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/featherlight.gallery.css">                 
    <!--[if lt IE 9]>
    <script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo $this->uri->baseUri;?>assets/admin/js/jquery.1-8-2.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->uri->baseUri;?>assets/admin/js/featherlight.js" type="text/javascript"></script>
    <style>
        body {
            line-height: 15px !important;
        }
        #editor { 
                position: absolute;
                top: 45px;
                right: 0;
                bottom: 0;
                left: 0;
                z-index: -9999;
            }
        .lightbox { display: none; }
    </style>
</head>
<body>
<iframe class="lightbox" src="<?=$this->location('admin/template/preview')?>" width="1275" height="500" id="fl3" style="border:none;" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>
    <div class="navbar navbar-static-top">
       <div class="navbar-inner">
          <div class="container-fluid">
             <a class="brand" href="./">Themes Editor</a>
             <a class="btn btn-small" href="./"><i class="icon-home"></i></a>
             <a class="btn btn-info btn-small" href="<?=$this->location('admin/template/docs')?>" target="_blank"><i class="icon-list-alt"></i> Documentation</a>
             <a id="save" class="btn btn-success btn-small"><i class="icon-edit"></i> Save</a>
             <a id="preview" class="btn btn-warning btn-small"><i class="icon-eye-open"></i> Preview</a>
             <a id="reset" class="btn btn-small"><i class="icon-retweet"></i> Reset</a>
            <ul class="nav pull-right">
                <li>
                    <a style="padding: 5px 0 0 0;" href="#">
                    </a>
                </li>
                <li class="divider-vertical"></li>
                <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile <b class="caret"></b></a>
                    <ul class="dropdown-menu">
                        <li>
                            <a href="<?=$this->location('admin/password')?>">Change password</a>
                        </li>
                        <li class="divider"></li>
                        <li>
                            <a href="<?php echo $this->location('admin/auth/logout'); ?>">Sign out</a>
                        </li>
                    </ul>
                </li>
            </ul>
          </div>
       </div>
    </div>

    <div id="editor">
        <?=$template?>
    </div>
    <script src="<?php echo $this->uri->baseUri;?>assets/admin/js/bootstrap-2.min.js" type="text/javascript"></script>
    <script src="http://d1n0x3qji82z53.cloudfront.net/src-min-noconflict/ace.js" type="text/javascript" charset="utf-8"></script>
    <script>

        var editor = ace.edit("editor");

        editor.setTheme("ace/theme/eclipse");
        editor.getSession().setMode("ace/mode/html");

        $('#save').on('click', function() {

            var html = editor.getValue();

            $.post("<?=$this->location('admin/template/editor')?>", {data: html}, function(data, textStatus, xhr) {

                if(textStatus == 'success') {
                    alert("all saved!");
                }

            });

        });

        $('#reset').on('click', function() {
            $.post("<?=$this->location('admin/template/reset')?>", function(data, textStatus, xhr) {
                if(data.data == true) {
                    alert('Reset to default done!');
                    location.reload();
                } else {
                    alert('Ops! something is wrong :(');
                }
            });
        });

        $('#preview').on('click', function() {
                 
            var html = editor.getValue();

            $.post("<?=$this->location('admin/template/preview')?>", {data: html}, function(data, textStatus, xhr) {

                if(textStatus == 'success') {
                    $.featherlight('#fl3', {
                        // closeOnEsc: false,
                        // closeOnClick: false,
                        afterClose: function(){
                            $.post('<?=$this->location('admin/template/preview')?>?removePreview', function(data, textStatus, xhr) {
                                if(textStatus == 'success')
                                console.log('previewRemoved');
                            });
                        }
                    });                    
                }

            });

        });

    </script>    
</body>
</html>