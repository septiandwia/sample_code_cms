<script type="text/javascript">
	
	$(document).ready(function() {

		$("#checkAll").click(function(){
		    $('input:checkbox').not(this).prop('checked', this.checked);
		});

		$('#deleteSelected').on('click', function() {
			var k = confirm("Apakah anda yakin ingin menghapus produk ini?");
			if(k) {			
				var checkProduk = "";
				$('.checkProduk').each(function() {
					var isChecked = $(this).is(":checked");
					if(isChecked) {
						var id = $(this).val();

							$.post('<?=$this->location('admin/product/delete')?>?id=' + id, function(data, textStatus, xhr) {
								if(textStatus == 'success') {
									$('#produk-' + data.id).fadeOut('slow', function() {
										$(this).remove();
									});
								} else {
									alert("Ops terjadi kesalahan!");
								}
							});
					}
				});
			}
		});

		$('.delete').on('click', function() {
			var id = $(this).attr('data-url');
			var k = confirm("Apakah anda yakin ingin menghapus produk ini?");
			if(k) {
				$.post('<?=$this->location('admin/product/delete')?>?id=' + id, function(data, textStatus, xhr) {
					if(textStatus == 'success') {
						$('#produk-' + data.id).fadeOut('slow', function() {
							$(this).remove();
						});
					} else {
						alert("Ops terjadi kesalahan!");
					}
				});
			}
		});
	});

</script>
<h4>Product</h4>
<hr>
<div class="btn-toolbar">
	<div class="btn-group">
		<a href="<?php echo $this->location('admin/product/edit'); ?>" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Add Product</a>
	</div>
	<div class="btn-group">
		<a id="deleteSelected" href="#" class="btn btn-warning"><i class="fa fa-trash" aria-hidden="true"></i> Delete Selected</a>
	</div>
	<div class="btn-group">
		<a href="<?=$this->location('admin/product')?>" class="btn btn-info"><i class="fa fa-refresh" aria-hidden="true"></i> Refresh</a>
	</div>
</div>

<div class="row-fluid">

			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th><input id="checkAll" type="checkbox" /></th>
						<th>#</th>
						<th>Nama Produk</th>
						<th>Berat gram(s)</th>
						<th>Harga</th>
						<th>SKU</th>
						<th>Stok</th>
						<th>Tgl. Masuk</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($products as $product): ?>
					<tr id="produk-<?=$product->id_produk?>">
						<td><input class="checkProduk" name="check[]" value="<?=$product->id_produk?>" type="checkbox"></td>
						<td><?=$product->id_produk?></td>
						<td><?=$product->nama_produk?></td>
						<td><?=number_format($product->berat,0,',','.')?></td>
						<td><?=number_format($product->harga,0,',','.')?></td>
						<td><?=$product->sku?></td>
						<td><?=$product->stok?></td>
						<td><?=date("j F Y", strtotime($product->tgl_masuk))?></td>  
						<td>
						    <div class="btn-group">
						      <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						        Action
						        <span class="caret"></span>
						      </a>
						      <ul class="dropdown-menu">
						        <li><a href="<?=$this->location('admin/product/edit?id='.$product->id_produk)?>">Edit</a></li>
						        <li><a class="delete" href="#" data-url="<?=$product->id_produk?>">Delete</a></li>
						      </ul>
						    </div>
						</td>												
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>
	<?php if($pageLinks): ?>
    <div class="pagination">
      <ul>
      <?php foreach($pageLinks as $paging): ?>
      	<li><?php echo $paging; ?></li>
      <?php endforeach; ?>
      </ul>
    </div>
	<?php endif; ?>