<script type="text/javascript">
	$(document).ready(function() {
		$('.delete').on('click', function() {
			var id = $(this).attr('data-url');
			var k = confirm("Apakah anda yakin ingin menghapus data ini?");
			if(k) {
				$.post('<?=$this->location('admin/payment/delete')?>?id=' + id, function(data, textStatus, xhr) {
					if(textStatus == 'success') {
						$('#bank-' + data.id).fadeOut('slow', function() {
							$(this).remove();
						});
					} else {
						alert("Ops terjadi kesalahan!");
					}
				});
			}
		});
	});	
</script>
<h4>Product</h4>
<hr>
<div class="btn-toolbar">
	<div class="btn-group">
		<a href="<?php echo $this->location('admin/payment/edit'); ?>" class="btn btn-success"><i class="fa fa-plus-circle" aria-hidden="true"></i> Tambah Rekening</a>
	</div>
</div>

<div class="row-fluid">

			<table class="table table-hover table-striped">
				<thead>
					<tr>
						<th>Nama Bank</th>
						<th>No. Rekening</th>
						<th>Pemegang Rekening</th>
						<th>Aksi</th>
					</tr>
				</thead>
				<tbody>
				<?php foreach($rek as $v): ?>
					<tr id="bank-<?=$v->id_bank?>">
						<td><strong><i><?=$v->nama_bank?></i></strong></td>
						<td><?=$v->no_rekening?></td>
						<td><?=$v->pemilik?></td>
						<td>
						    <div class="btn-group">
						      <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
						        Action
						        <span class="caret"></span>
						      </a>
						      <ul class="dropdown-menu">
						        <li><a class="delete" href="#" data-url="<?=$v->id_bank?>">Delete</a></li>
						      </ul>
						    </div>
						</td>						
					</tr>
				<?php endforeach; ?>
				</tbody>
			</table>
		</div>	