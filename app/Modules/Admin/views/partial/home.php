<script src="<?=$this->uri->baseUri;?>assets/admin/js/highchart.js?>" type="text/javascript"></script>
<h4>
   Overview
</h4>
<hr>
<div class="row-fluid">
   <div class="span8">
      <div class="row-fluid">
         <div class="row-fluid">
            <h4><i class="icon-bar-chart"></i> Statistics</h4>
         </div>
         <div class="row-fluid">
         	<div id="container" style="width:100%; height:400px;"></div>
         </div>
      </div>

   </div>
   <div class="span4">
      <div class="row-fluid">
         <div class="box-title">
            <h4>
               <i class="icon-tasks"></i>
               Review
            </h4>
         </div>
         <div class="row-fluid">
            <style>table>tbody>tr>td:last-child{text-align:right;}</style>
            <table class="table table-condensed">
               <tbody>
                  <tr>
                     <td>Total Sales This Year</td>
                     <td>Rp. <span id="salesThisYear"><?=$getTotalHarga?>,-</span></td>
                  </tr>
                  <tr>
                     <td>Total Sales This Month</td>
                     <td>Rp. <span id="salesThisMonth"><?=$getTotalSalesMonth?>,-</span></td>
                  </tr>
                  <tr>
                     <td>Total Orders</td>
                     <td><?=$getTotalOrder?></td>
                  </tr>
                  <tr>
                     <td>No. of Customers</td>
                     <td id="customers"><?=$jmlKustomer?></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
      <div class="row-fluid">
         <div class="box-title">
            <h4>
               <i class="icon-random"></i>
               Info
            </h4>
         </div>
         <div class="row-fluid">
            <table class="table table-condensed">
               <tbody id="topReferral">No data available</tbody>
            </table>
         </div>
      </div>
   </div>
</div>
      <div class="row-fluid">
         <div class="row-fluid">
            <h4>
               <i class="icon-shopping-cart"></i>
               Latest Orders
            </h4>
         </div>
         <div class="row-fluid">

          <table class="table table-hover table-striped">
            <thead>
              <tr>
                <th>#ID</th>
                <th>Nama Kustomer</th>
                <th>Telepon</th>
                <th>Email</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach($latestOrder as $v): ?>
              <tr>
                <td>00<?=$v->id_orders?></td>
                <td><?=$v->nama_lengkap?></td>
                <td><?=$v->telpon?></td>
                <td><?=$v->email?></td>
                <td>
                    <div class="btn-group">
                      <a class="btn btn-<?=($v->status_order == 'baru') ? 'warning' : 'success'?> dropdown-toggle" data-toggle="dropdown" href="#">
                        <?=($v->status_order == 'baru') ? 'Baru' : 'Lunas'?>
                        <span class="caret"></span>
                      </a>
                      <ul class="dropdown-menu">
                      <li><a href="<?=$this->location('admin/order/detail')?>?id=<?=$v->id_orders?>">Lihat</a></li>
                      </ul>
                    </div>          
                </td>
              </tr>
            <?php endforeach; ?>
            </tbody>
          </table>
         </div>
      </div>
<script type="text/javascript">
$(function () { 
    var myChart = Highcharts.chart('container', {
        chart: {
            type: 'line'
        },
        title: {
            text: 'Jumlah Penjualan Bulan ini'
        },
        xAxis: {
            categories: <?=($dayOrder) ? $dayOrder : '[Belum ada data]'?>
        },
        yAxis: {
            title: {
                text: 'Jumlah Penjualan'
            }
        },
        series: [
          {
              name: 'Penjualan',
              data: <?=($jmlDayOrder) ? $jmlDayOrder : '[0]'?>
          }                    
        ]
    });
});	
</script>
