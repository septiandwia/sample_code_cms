

<h4>Your active template: E-Commerce Bootstrap</h4>
<hr>
<div class="row-fluid">
   <div class="span">
      <div class="row-fluid">
         <ul class="thumbnails">
            <li class="span4">
               <div class="thumbnail">
                  <img alt="" src="<?=$this->uri->baseUri;?>template/screenshot.png">
               </div>
               <h5>E-Commerce Bootstrap</h5>
               <p><a href="<?=$this->location('admin/template/editor')?>" class="btn btn-info btn-edit">Edit HTML</a></p>
            </li>
         </ul>
      </div>
   </div>
</div>
