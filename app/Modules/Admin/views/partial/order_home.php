<h4>List Order</h4>
<hr>
<div class="row-fluid">

	<table class="table table-hover table-striped">
		<thead>
			<tr>
				<th>#ID</th>
				<th>Nama Kustomer</th>
				<th>Telepon</th>
				<th>Email</th>
				<th>Status</th>
			</tr>
		</thead>
		<tbody>
		<?php foreach($orders as $v): ?>
			<tr>
				<td>00<?=$v->id_orders?></td>
				<td><?=$v->nama_lengkap?></td>
				<td><?=$v->telpon?></td>
				<td><?=$v->email?></td>
				<td>
				    <div class="btn-group">
				      <a class="btn btn-<?=($v->status_order == 'baru') ? 'warning' : 'success'?> dropdown-toggle" data-toggle="dropdown" href="#">
				        <?=($v->status_order == 'baru') ? 'Baru' : 'Lunas'?>
				        <span class="caret"></span>
				      </a>
				      <ul class="dropdown-menu">
				      <li><a href="<?=$this->location('admin/order/detail')?>?id=<?=$v->id_orders?>">Lihat</a></li>
				      </ul>
				    </div>					
				</td>
			</tr>
		<?php endforeach; ?>
		</tbody>
	</table>

</div>
	<?php if($pageLinks): ?>
    <div class="pagination">
      <ul>
      <?php foreach($pageLinks as $paging): ?>
      	<li><?php echo $paging; ?></li>
      <?php endforeach; ?>
      </ul>
    </div>
	<?php endif; ?>