<h4>Detail Order</h4>
<hr>
<form method="POST">
   <input name="id" value="32" type="hidden">
   <table class="detailOrder">
      <thead>
         <tr>
            <th colspan="2">Overview</th>
         </tr>
      </thead>
      <tbody>
         <tr>
            <td>No. Order</td>
            <td> : <strong><?=$id_order?></strong></td>
         </tr>
         <tr>
            <td>Tgl. &amp; Jam Order</td>
            <td> : <?=$tgl_order?></td>
         </tr>
         <tr>
            <td>Status Order</td>
            <td>
                <div class="btn-group">
                  <a class="btn btn-<?=($status_order == 'baru') ? 'warning' : 'success'?> dropdown-toggle" data-toggle="dropdown" href="#">
                    <?=($status_order == 'baru') ? 'Belum Lunas' : 'Sudah Lunas'?>
                    <span class="caret"></span>
                  </a>
                  <ul class="dropdown-menu">
                     <?=($status_order == 'baru') ? '<li><a href="'.$this->location('admin/order/edit').'?id='.$id_order.'&status=lunas">Sudah Lunas</a></li>' : '<li><a href="?id='.$id_order.'&status=belum_lunas">Belum Lunas</a></li>'?>
                  </ul>
                </div>            
            </td>
         </tr>
      </tbody>
   </table>
</form>
<table class="detailOrder">
   <tbody>
      <tr>
         <th>Nama Produk</th>
         <th>Berat(grams)</th>
         <th>Jumlah</th>
         <th>Harga Satuan</th>
         <th>Sub Total</th>
      </tr>
      <?php
         $subtotal = 0;
         foreach($details as $val):
            $subtotal += $val['subtotal'];
      ?>   
      <tr>
         <td style="text-align: left;" align="left"><?=$val['name']?></td>
         <td style="text-align: center;" align="left"><?=$val['weight']?></td>
         <td style="text-align: center;" align="left"><?=$val['quantity']?></td>
         <td style="text-align: center; padding-right: 20px;" align="right">Rp. <?=number_format($val['price'],2,',','.')?></td>
         <td style="text-align: center; padding-right: 20px;" align="right">Rp. <?=number_format($val['subtotal'],2,',','.')?></td>
      </tr>
      <?php endforeach; ?>
      <tr>
         <td colspan="4">
            <strong><?=$jenis_pengiriman?></strong>
         </td>
         <td style="text-align: center; padding-right: 20px;" align="right">Rp. <?=number_format($harga_pengiriman,2,',','.')?></td>
      </tr>
      <tr>
         <td style="text-align: right;" colspan="4">
            <strong>Total Harga:</strong>
         </td>
         <td style="text-align: center; padding-right: 20px; font-weight: bold;" align="right">Rp. <?=number_format($harga_pengiriman + $subtotal,2,',','.')?></td>
      </tr>      
   </tbody>
</table>

<table class="detailOrder">
   <tbody>
      <tr>
         <th colspan="2">Data Kustomer</th>
      </tr>
      <tr>
         <td>Nama Kustomer</td>
         <td> : <?=$nama_kustomer?></td>
      </tr>
      <tr>
         <td>Alamat Pengiriman</td>
         <td> : <?=$alamat?>, Provinsi: <?=$alamat_ongkir->province?>, <?=$alamat_ongkir->type?> <?=$alamat_ongkir->city_name?> - POSTAL CODE : <?=$alamat_ongkir->postal_code?></td>
      </tr>
      <tr>
         <td>No. Telpon/HP</td>
         <td> : <?=$telpon?></td>
      </tr>
      <tr>
         <td>Email</td>
         <td> : <?=$email?></td>
      </tr>
   </tbody>
</table>

