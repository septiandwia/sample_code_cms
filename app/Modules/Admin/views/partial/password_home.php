<h4>Password</h4>
<hr>
<div class="row-fluid">
<?php if(isset($status)): ?>
<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><?=$status?></div>
<?php endif; ?>
   <form id="formKategori" action="" method="post" class="form-horizontal">
      <div class="control-group">
         <label class="control-label" for="password">Rubah Password : </label>
         <div class="controls">
            <input name="password" id="password" placeholder="Masukan password baru..." value="" type="password" />
         </div>
      </div>
   </form>

   	<div class="form-horizontal">
        <div class="form-actions">
         <div class="btn-toolbar">
            <div class="btn-group">
               <button id="save" name="save" class="btn btn-primary">Save changes</button>
               <a href="<?=$this->location('admin/dashboard')?>" class="btn">Cancel</a>
            </div>
         </div>
      </div> 
    </div>

</div>