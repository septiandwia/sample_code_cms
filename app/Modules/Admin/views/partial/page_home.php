<script type="text/javascript">

   $(document).on('click', '.publish', function(e) {

         var id = $(this).attr('data-url');
         $.post('<?=$this->location('admin/page/publish')?>?id=' + id, function(data, textStatus, xhr) {
            if(textStatus == 'success') {
               $('#publishTd-' + data.id).empty();
               $('#publishTd-' + data.id).append('Published');
               $('#publishTd-' + data.id).css('color', '');
               $('#publishTd-' + data.id).css('color', 'green');
               $('#btnPublish-' + data.id).remove();
               $('#listAksi-' + data.id).append('<li id="btnNotPublish-'+data.id+'"><a data-url="'+data.id+'" class="notPublish" href="#">Dont Publish</a></li>');
            } else {
               alert('Ops terjadi kesalahan!');
            }
         });

   });

   $(document).on('click', '.notPublish', function(e) {

         var id = $(this).attr('data-url');
         $.post('<?=$this->location('admin/page/dontpublish')?>?id=' + id, function(data, textStatus, xhr) {
            if(textStatus == 'success') {
               $('#publishTd-' + data.id).empty();
               $('#publishTd-' + data.id).append('Not Published');               
               $('#publishTd-' + data.id).css('color', '');
               $('#publishTd-' + data.id).css('color', 'red');
               $('#btnNotPublish-' + data.id).remove();
               $('#listAksi-' + data.id).append('<li id="btnPublish-'+data.id+'"><a data-url="'+data.id+'" class="publish" href="#">Publish</a></li>');               
            } else {
               alert('Ops terjadi kesalahan!');
            }
         });

   });   
   
   $(document).ready(function() {

      $("#checkAll").click(function(){
          $('input:checkbox').not(this).prop('checked', this.checked);
      });

      $('.delete').on('click', function() {

         if(confirm('Apakah anda yakin untuk menghapus halaman ini?'))
         {
            var id = $(this).attr('data-url');
            
            $.post('<?=$this->location('admin/page/delete')?>?id=' + id, function(data, textStatus, xhr) {
               
               if(textStatus == 'success') {
                  $('#page-' + data.id).fadeOut('slow', function() {
                     $(this).remove();
                  });
               } else {
                  alert('Ops terjadi kesalahan!');
               }

            });
         }

      });

      $('#deleteSelected').on('click', function() {
         
         var k = confirm("Apakah anda yakin ingin menghapus page ini?");
         
         if(k) {        
            
            var checkPage = "";
            
            $('.checkPage').each(function() {
               
               var isChecked = $(this).is(":checked");

               if(isChecked) {

                  var id = $(this).val();

                  $.post('<?=$this->location('admin/page/delete')?>?id=' + id, function(data, textStatus, xhr) {
                     if(textStatus == 'success') {
                        $('#page-' + data.id).fadeOut('slow', function() {
                           $(this).remove();
                        });
                     } else {
                        alert("Ops terjadi kesalahan!");
                     }
                  });
                  
               }
            });
         }
      });

   });

</script>
<h4>Pages</h4>
<hr>
<div class="row-fluid">
   <div class="span">
      <div class="row-fluid">
         <div class="btn-toolbar">
            <div class="btn-group">
               <a href="<?=$this->location('admin/page/edit')?>" class="btn">Add</a>
            </div>
            <div class="btn-group pbl">
               <a id="deleteSelected" class="btn" href="#"><i class="icon-trash"></i> Delete Selected</a>
            </div>
         </div>
         <hr>
         <div class="row-fluid">
            <table class="table table-hover">
               <thead>
                  <tr>
                     <th><input id="checkAll" type="checkbox" /></th>
                     <th>Judul</th>
                     <th>Status</th>
                     <th>Tanggal Dibuat</th>
                     <th>Aksi</th>
                  </tr>
               </thead>
               <tbody>
               <?php foreach($pages as $page): ?>
                  <tr id="page-<?=$page->id_berita?>">
                     <td><input name="check[]" value="<?=$page->id_berita?>" class="checkPage" type="checkbox" /></td>
                     <td class="right"><a href="<?=$this->location('admin/page/edit/'.$page->id_berita)?>"><?=$page->title?></a></td>
                     <td id="publishTd-<?=$page->id_berita?>" style="color: <?=($page->publish == 1) ? 'green' : 'red'; ?>"; class="left"><?=($page->publish == 1) ? 'Published' : 'Not Published'; ?></td>
                     <td class="left"><?=$page->tanggal?></td>
                     <td>
                         <div class="btn-group">
                           <a class="btn dropdown-toggle" data-toggle="dropdown" href="#">
                             Action
                             <span class="caret"></span>
                           </a>
                           <ul id="listAksi-<?=$page->id_berita?>" class="dropdown-menu">
                             <li><a style="color: blue;" href="<?=$this->location('admin/page/edit/'.$page->id_berita)?>">Edit</a></li>
                             <?php if($page->publish != 1): ?>
                             <li id="btnPublish-<?=$page->id_berita?>"><a href="#" class="publish" data-url="<?=$page->id_berita?>">Publish</a></li>
                              <?php else: ?>
                             <li id="btnNotPublish-<?=$page->id_berita?>"><a href="#" class="notPublish" data-url="<?=$page->id_berita?>">Dont Publish</a></li>
                              <?php endif; ?>
                             <li><a class="delete" href="#" data-url="<?=$page->id_berita?>"><font color="red"> Delete</font></a></li>
                           </ul>
                         </div>
                     </td>
                  </tr>
               <?php endforeach; ?>
               </tbody>
            </table>
         </div>
         <div class="pagination pagination-right"></div>
      </div>
   </div>
</div>

