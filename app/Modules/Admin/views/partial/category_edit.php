<link rel="stylesheet" type="text/css" href="<?=$this->uri->baseUri?>assets/admin/css/chosen.min.css" />
<script src="<?=$this->uri->baseUri?>assets/admin/js/chosen.jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(window).load(function(){
        $(".chosen-select").chosen()
    }); 
</script>

<script type="text/javascript">

	$(document).ready(function() {

		$('#save').on('click', function() {

			var data = $("#formKategori").serializeArray();

	        $.ajax({
	            type: "POST",
	            url: "<?=$this->location('admin/category/edit')?>",
	            data: data,
	            success: function(data) {
                  $('.alert').remove();
	            	$('#alert').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info</strong> Kategori '+data.name+' berhasil ditambahkan!</div>');
	            }
	        });

		});
		
	});
	
</script>
<h4>Update categories</h4>
<div id="alert"></div>
<hr>
<div class="row-fluid">
   <form id="formKategori" class="form-horizontal">
      <?php if(isset($category->id_kategori)): ?>
        <input value="<?=$category->id_kategori?>" name="id" type="hidden" />
      <?php endif;?>
      <div class="control-group">
         <label class="control-label" for="name">Name</label>
         <div class="controls">
            <input name="name" id="name" placeholder="Category name" value="<?=(isset($category->nama_kategori)) ? $category->nama_kategori : ''?>" type="text" />
         </div>
      </div>
      <div class="control-group">
         <label class="control-label" for="parent">Parent category (optional)</label>
         <div class="controls">
            <select class="chosen-select" id="parent" name="kategori">
            <?php if(isset($category->id_kategori)): ?>
              <?php foreach($categories as $val): ?>
                <option value="<?=$val->id_kategori?>" <?=($category->id_kategori == $val->id_kategori) ? 'selected' : ''?>><?=$val->nama_kategori?> - (<?=$val->jmlProduk?> Produk)</option>
              <?php endforeach; ?>
            <?php else: ?>
            <option value="0"></option>
            <?php echo $categories; ?>
          <?php endif; ?>
            </select>          
         </div>
      </div>
   </form>
   	<div class="form-horizontal">
        <div class="form-actions">
         <div class="btn-toolbar">
            <div class="btn-group">
               <button id="save" name="save" value="1" class="btn btn-primary">Save changes</button>
               <a href="<?=$this->location('admin/category')?>" class="btn">Cancel</a>
            </div>
         </div>
      </div> 
    </div>
</div>

