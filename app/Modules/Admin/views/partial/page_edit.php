<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    height: 270,
    theme: 'modern',
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | fontsizeselect | forecolor backcolor',
    image_advtab: true
   });
</script>

<script type="text/javascript">
   
   $(document).ready(function() {
      
      $('#save').on('click', function() {

         tinyMCE.triggerSave();
         var contents = $("#contentPage").serializeArray();

           $.ajax({
               type: "POST",
               url: "<?=$this->location('admin/page/edit')?>",
               data: contents,
               success: function(data) {
                     console.log(data);
                     var tinymce_editor_id = 'ajaxfilemanager';
                     tinymce.get(tinymce_editor_id).setContent('');                
                     $('#title').val('');
                     $('#ajaxfilemanager').val('');
                     $('html,body').scrollTop(0);
                     $('#alert').empty();
                     $('#alert').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info</strong> Data Berhasil di Save!</div>');
                     $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                        $(".alert").slideUp(500);
                     }); 
               }
           });         

      });

   });

</script>

<h4>Update Page</h4>
<div id="alert"></div>
<hr>
<div class="row-fluid">
   <form id="contentPage">
      <?php if(isset($page)): ?>
         <input name="id_berita" value="<?=$page->id_berita?>" type="hidden" />
      <?php endif; ?>
      <div class="control-group">
         <div class="controls">
            <input autocomplete="off" class="input-block-level" name="title" id="title" placeholder="Page title" value="<?=(isset($page->judul)) ? $page->judul : '' ?>" type="text">
         </div>
      </div>
      <div class="control-group">
         <div class="controls">
            <textarea id="ajaxfilemanager" name="content" class="desc input-block-level" style="height: 500px;" placeholder="Page content"><?=(isset($page->isi_berita)) ? $page->isi_berita : '' ?></textarea>
         </div>
      </div>
      <div class="control-group">
         <label class="checkbox">
         <input name="isPublished" <?=(isset($page->publish) AND $page->publish == 1) ? 'checked' : '' ?> type="checkbox"> Publish
         </label>
      </div>
      <hr>
   </form>
      <div class="control-group">
         <div class="btn-toolbar">
            <div class="btn-group">
               <button id="save" class="btn btn-primary">Save changes</button>
            </div>
            <div class="btn-group">
               <a href="<?=$this->location('admin/page')?>" class="btn">Cancel</a>
            </div>
         </div>
      </div>
</div>

