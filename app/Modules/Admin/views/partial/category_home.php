<script type="text/javascript">
	$(document).ready(function() {
		$('.delCat').on('click', function() {
			if(confirm('Apakah anda yakin ingin menghapus kategori ini?')) {
				var id = $(this).attr('data-url');
				$.post('<?=$this->location('admin/category/delete')?>?id=' + id, function(data, textStatus, xhr) {
					console.log('#idCategory-' + data.id);
					if(textStatus == 'success') {
						$('#idCategory-' + data.id).remove();
					}
				});				
			}
		});
	});
</script>
<h4>Categories</h4>
<hr>
<div class="btn-toolbar">
	<div class="btn-group">
		<a href="<?php echo $this->location('admin/category/edit'); ?>" class="btn">Add Category</a>
	</div>
</div>

<?php echo $categories; ?>

