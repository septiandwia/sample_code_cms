<link rel="stylesheet" type="text/css" href="<?=$this->uri->baseUri?>assets/admin/css/chosen.min.css" />
<script src="<?=$this->uri->baseUri?>assets/admin/js/chosen.jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
    $(window).load(function(){
        $(".chosen-select").chosen()
    }); 
</script>

<script src="<?php echo $this->uri->baseUri;?>assets/admin/js/simpleUpload.min.js" type="text/javascript"></script>
<!--Tiny MCPUK-->
<script src="//cdn.tinymce.com/4/tinymce.min.js"></script>
<script>
  tinymce.init({
    selector: 'textarea',
    height: 270,
    theme: 'modern',
    plugins: [
      'advlist autolink lists link image charmap print preview hr anchor pagebreak',
      'searchreplace wordcount visualblocks visualchars code fullscreen',
      'insertdatetime media nonbreaking save table contextmenu directionality',
      'emoticons template paste textcolor colorpicker textpattern imagetools'
    ],
    toolbar1: 'insertfile | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist fontsizeselect | link image | forecolor backcolor',
    image_advtab: true
   });
</script>
    <!--end Tiny--> 
<script type="text/javascript">
   
    function imgError(gambar) {
        $('#preview-' + gambar).remove();
        $('#' + gambar).remove();
    }

$(document).ready(function(){    

	$('#save').on('click', function() {

		tinyMCE.triggerSave();
		var dataProduk = $("#dataProduk").serializeArray();
			dataProduk.push({name: 'publish', value: 0});

        $.ajax({
            type: "POST",
            url: "<?=$this->location('admin/product/edit')?>",
            data: dataProduk,
            success: function(data) {
                 // console.log(data);

                var tinymce_editor_id = 'ajaxfilemanager';
                tinymce.get(tinymce_editor_id).setContent('');    
                $('#ajaxfilemanager').val('');
                $('#prod-name').val('');
                $('#price').val('');
                $('#sku').val('');
                $('#weight').val('');
                $('#quantity').val('');
                $('html,body').scrollTop(0);
                $('#alert').empty();
                $('#alert').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info</strong> Data Berhasil di Save!</div>');
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert").slideUp(500);
                });

                setTimeout(function() {
                    location.reload();
                }, 2000);
                                                
            }
        });

	});

	$('#publish').on('click', function() {

		tinyMCE.triggerSave();
		var dataProduk = $("#dataProduk").serializeArray();
			dataProduk.push({name: 'publish', value: 1});

        $.ajax({
            type: "POST",
            url: "<?=$this->location('admin/product/edit')?>",
            data: dataProduk,
            success: function(data) {
                 // console.log(data);

                var tinymce_editor_id = 'ajaxfilemanager';
                tinymce.get(tinymce_editor_id).setContent('');    
                $('#ajaxfilemanager').val('');
                $('#prod-name').val('');
                $('#price').val('');
                $('#weight').val('');
                $('#sku').val('');
                $('#quantity').val('');                 
                $('html,body').scrollTop(0);
                $('#alert').empty();
                $('#alert').append('<div class="alert alert-success"><button type="button" class="close" data-dismiss="alert">&times;</button><strong>Info</strong> Data Berhasil di Publish!</div>');
                $(".alert").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert").slideUp(500);
                });

                setTimeout(function() {
                    location.reload();
                }, 2000);
            }
        });

	});	

	$('.preview').on('click', '.hapusImage', function() {

        if(confirm('Apakah anda yakin akan menghapus foto ini? \n File akan dihapus secara permanen!'))
        {
            $('.progress').hide();
            
            var url = $(this).attr('data-url');
            var fullUrl = url;
                url = url.split('/');
                url = url[url.length - 1];
            
            $.post('<?=$this->location('admin/upload/delete')?>', {data: fullUrl}, function(data, textStatus, xhr) {
                
                if(textStatus == 'success') {
            
                    var id = data.file.split('/');
                        id = id[3];
                        id = id.split('.');
                        id = id[0];             
                        $('#' + id).remove();
                        $('#preview-' + id).remove();
                        $('#progress').hide();
                        $('#progressBar').hide();

                } else {
                    console.log(textStatus);
                }

            });            
        }
	
	});

	$('#fileupload').change(function(){

		$(this).simpleUpload("<?php echo $this->location('admin/upload'); ?>", {

			start: function(file){
				//upload started
				$('#filename').html(file.name);
				$('#progress').html("");
				$('#progressBar').width(0);
			},

			progress: function(progress){
				//received progress
				$('#progressBar').show();
                $('.progress').show();
				$('#progress').html("Progress: " + Math.round(progress) + "%");
				$('#progressBar').width(progress + "%");
			},

			success: function(data){

				var idInput = data.name;
					idInput = idInput.split('.');
					idInput = idInput[idInput.length - 2];

                $('#progressBar').remove();
                $('#progress').remove();
                $('.progress').remove();

				//upload successful
				$('.preview').append('<div id="preview-' + idInput + '" class="imgP"><img src=' + data.folder.replace('.','<?=$this->uri->baseUri?>') + data.name + ' /><br clear="all" /><span data-url="' + data.folder.replace('.','<?=$this->uri->baseUri?>') + data.name + '" class="hapusImage">Delete</span></div>');
				$('form').append('<input id="' + idInput + '" class="inpImage" type="hidden" name="foto[]" value="' + data.folder.replace('.','<?=$this->uri->baseUri?>') + data.name + '" />');
			},

			error: function(error){
				//upload failed
				$('#progress').html("Failure!<br>" + error.name + ": " + error.message);
			}

		});

	});

});

</script>

<link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/fileupload.css">
    <div class="row-fluid">
        <div class="pull-left">
            <h4>Add product</h4>
        </div>
        <div style="float: left; clear: both;" id="alert"></div>
        <div class="pull-right buttons">
            <button id="save" class="btn btn-inverse" type="button" value="Save">Save</button>
            <button id="publish" class="btn" type="button" value="Publish">Publish</button>
            <a href="<?=$this->location('admin/product')?>" class="btn">Close</a>
        </div>
    </div>
    <hr>
    <div class="row-fluid">
        <div class="span8">
            <div class="row-fluid">
                <form id="dataProduk">
                    <?php if(isset($produk)): ?>
                        <input value="1" name="update" type="hidden" />
                        <input value="<?=$produk->id_produk?>" name="id_produk" type="hidden" />
                    <?php endif; ?>
                    <div class="control-group">
                        <label class="control-label" for="prod-name">Judul</label>
                        <div class="controls">
                            <input id="prod-name" name="prod-name" class="input-block-level" placeholder="Masukan nama produk" autocomplete="off" value="<?=(isset($produk)) ? $produk->nama_produk : ''?>" type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label>Deskripsi</label>
                        <textarea name="deskripsi" id="ajaxfilemanager" class="desc input-block-level" style="height: 270px;"><?=(isset($produk)) ? $produk->deskripsi : ''?></textarea>
                    </div>
            </div>
            <div class="row-fluid">
                <h5>Foto Produk</h5>
				<div id="progress"></div>
				<!-- <div id="progressBar"></div> -->
                <div style="display: none;" class="progress progress-striped active">
                  <div id="progressBar" class="bar"></div>
                </div>
                <hr>
                <div class="row-fluid">
					<div class="preview">
                    <?php if(isset($produk)): ?>
                        <?php
                            foreach($produk->gambar as $foto):
                                $idFoto = explode(".", $foto);
                                $idFoto = explode("/", $idFoto[0]);
                                $idFoto = end($idFoto);
                        ?>
                            <input id="<?=$idFoto?>" class="inpImage" type="hidden" name="foto[]" value="<?=$foto?>" />
                            <div id="preview-<?=$idFoto?>" class="imgP"><img onerror="imgError('<?=$idFoto?>')" src="<?=$foto?>" /><br clear="all" /><span data-url="<?=$foto?>" class="hapusImage">Delete</span></div>
                        <?php endforeach; ?>
                    <?php endif; ?>
                    </div>
					<br clear="all" />     
                    <span class="btn btn-inverse fileinput-button">
					<i class="fa fa-plus fa fa-white"></i>
					<span>Upload Foto</span>
					<input id="fileupload" type="file" name="files">					
                    </span>
                </div>
              
            </div>

        </div>
        <div class="span4">
            <div class="row-fluid">
                Inventory
                <hr>
                <div class="form-horizontal inventory">
                    <div class="control-group">
                        <label class="control-label" for="price"><i class="fa fa-money"></i> Harga (IDR)</label>
                        <div class="controls">
                            <input name="harga" class="setMF" id="price" value="<?=(isset($produk)) ? $produk->harga : ''?>" type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="sku"><i class="fa fa-check"></i> sku</label>
                        <div class="controls">
                            <input name="sku" id="sku" value="<?=(isset($produk)) ? $produk->sku : ''?>" type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="quantity"><i class="fa fa-plus"></i> Stok Barang</label>
                        <div class="controls">
                            <input name="quantity" id="quantity" value="<?=(isset($produk)) ? $produk->stok : ''?>" type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="weight"><i class="fa fa-dashboard"></i> Berat (gram)</label>
                        <div class="controls">
                            <input name="berat" id="weight" value="<?=(isset($produk)) ? $produk->berat : ''?>" type="text">
                        </div>
                    </div>
                    <div class="control-group">
                        <label class="control-label" for="category"><i class="fa fa-tags"></i> Kategori</label>
                        <div class="controls category">
                        <select id="category" data-placeholder="Add a category..." name="kategori" class="chosen-select" style="width:96%;">
                        <option disabled value="0">Pilih Kategori</option>
                        <?php
                            if(isset($categories)):
                                foreach($categories as $key => $val):
                                    if(isset($val->childs)){
                                        echo '<optgroup label="'.$val->nama_kategori.'">';
                                        foreach($val->childs as $child){
                                            echo '<option value="'.$child->id_kategori.'">'.$child->nama_kategori.'</option>';
                                        }
                                        echo '</optgroup>';
                                    } else {
                                        echo '<option value="'.$val->id_kategori.'">'.$val->nama_kategori.'</option>';
                                    }
                                endforeach; 
                            endif;
                        ?>
                        </select>
                        </div>
                    </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
