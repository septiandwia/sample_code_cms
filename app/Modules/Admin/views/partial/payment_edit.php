
<div class="row-fluid">
   <form enctype="multipart/form-data" action="" method="post">
      <div class="span">
         <div class="bankInfo" style="">
            <div class="control-group">
               <label class="control-label">Nama Bank:</label>
               <div class="controls">
                  <input autocomplete="off" class="ui-autocomplete-input" id="namaBank" name="namaBank" value="" type="text">
               </div>
            </div>
            <div class="control-group">
               <label class="control-label">No. Rekening:</label>
               <div class="controls">
                  <input id="noRekening" name="noRekening" value="" type="number">
               </div>
            </div>
            <div class="control-group">
               <label class="control-label">Atas Nama:</label>
               <div class="controls">
                  <input id="atasNama" name="atasNama" value="" type="text">
               </div>
            </div>
            <div class="control-group">
               <label class="control-label">Upload Logo:</label>
               <div class="controls">
                  <input id="logoBank" name="logoBank" value="" type="file">
               </div>
            </div>            
         </div>
         <div class="control-group">
            <div class="form-actions">
               <button type="submit" class="btn btn-inverse save">Save changes</button>
               <a href="<?=$this->location('admin/payment')?>" class="btn">Cancel</a>
            </div>
         </div>
      </div>
   </form>
</div>
<!--/row-->
<link rel="stylesheet" type="text/css" href="<?=$this->uri->baseUri?>assets/admin/css/jquery.ui.custom.min.css">
<script src="<?=$this->uri->baseUri?>assets/admin/js/jquery-ui.min.js" type="text/javascript"></script>
<script type="text/javascript">
$(function(){

    var cnfg = {banks:false};
    
    loadBanks = function(){
   
      if (cnfg.banks)
          return;
      
      $.ajax({
          'async': false,
          'url': '<?=$this->uri->baseUri?>/assets/admin/js/bank.json',
          'dataType': 'json',
          'success': function(data){
         cnfg.banks = data;
          }
      });
      
      $('#namaBank').autocomplete({
          source:cnfg.banks,
          delay:100
      });
    }

    loadBanks()
    
});
</script>   