                <h4>Update categories</h4>
                <hr>
                <div class="row-fluid">
                    <form action="" method="post" class="form-horizontal">
                        <div class="control-group">
                            <label class="control-label" for="name">Name</label>
                            <div class="controls">
                                <input type="text" name="name" id="name" placeholder="Category name" value="">
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label" for="parent">Parent category (optional)</label>
                            <div class="controls">
                                <select id="parent" name="parent">
                                    <option></option>
                                    <option value="57d7826e96c03a450d8b457f">Kategori Satu</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-actions">
                            <div class="btn-toolbar">
                                <div class="btn-group">
                                    <button type="submit" name="save" value="1" class="btn">Save changes</button>
                                    <a href="/panel/site/siapbelajar/categories" class="btn">Cancel</a>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>