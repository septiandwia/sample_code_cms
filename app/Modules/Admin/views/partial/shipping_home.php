<link rel="stylesheet" type="text/css" href="<?=$this->uri->baseUri?>assets/admin/css/chosen.min.css" />
<script src="<?=$this->uri->baseUri?>assets/admin/js/chosen.jquery.min.js" type="text/javascript"></script>
<script type="text/javascript">
	$(window).load(function(){
		$(".chosen-select").chosen()
	});	
</script>
<h4>Shipping dan Ongkos kirim</h4>
<h3>Lokasi pengiriman saat ini: <strong><i><?=($cityName) ? $cityName['city'] : 'Belum di set :(' ?></i></strong></h3>
<hr>
<p>Shipping dan Ongkos kirim kami integrasikan dengan server kami.<br/>Untuk itu kami perlu mengetahui lokasi anda untuk menghitung destinasi barang yang dikirim sesuai dengan lokasi anda.</p>
<p>Kurir yang tersedia untuk saat ini adalah: <strong>TIKI</strong>, <strong>JNE</strong> dan <strong>POS INDONESIA</strong></p>

<div class="row-fluid">
   <form enctype="multipart/form-data" action="" method="post">
      <div class="span">
         <div class="bankInfo" style="">
            <div class="control-group">
               <label class="control-label">Provinsi:</label>
               <div class="controls">
		          <select name="province" id="province" data-placeholder="Choose a Country..." class="chosen-select" style="width:300px;" tabindex="2">

		          </select>
               </div>
            </div>
            <div class="control-group">
               <label class="control-label">Nama Kota:</label>
               <div class="controls">

		          <select name="city" id="city" data-placeholder="Choose a Country..." class="chosen-select" style="width:300px;" tabindex="2">

		          </select>

               </div>
            </div>
         </div>
         <div class="control-group">
            <div class="form-actions">
               <button type="submit" class="btn btn-inverse save">Save changes</button>
               <a href="<?=$this->location('admin/payment')?>" class="btn">Cancel</a>
            </div>
         </div>
      </div>
   </form>
</div>
<script type="text/javascript">
        
        $('#province').append('<option id="loadingProvince" value="0">Loading Province...</option>');

        $.post('<?=$this->uri->baseUri?>checkout/getprovince', function(data, textStatus, xhr) {

          if(textStatus == 'success') {
            $('#loadingProvince').remove();
            $('#province').append('<option value="0">--Select Province--</option>');
            $.each(data.rajaongkir.results, function(i, item) {
               $('#province').append('<option value="'+ item.province_id +'">'+item.province+'</option>');
            });
            $('#province').trigger("chosen:updated");
          }

        });

        $('#province').on('change', function() {

          $('#city').empty();
          
          var idprovince = this.value;          
          
          $('#city').append('<option id="loadingCity" value="0">Loading City...</option>');

          $.post('<?=$this->uri->baseUri?>checkout/getcity', {id_province: idprovince}, function(data, textStatus, xhr) {

            if(textStatus == 'success') {
              $('#loadingCity').remove();
              $('#city').append('<option value="0">--Select City--</option>');
              $.each(data.rajaongkir.results, function(i, item) {
                $('#city').append('<option value="'+ item.city_id +'-'+ item.type +'_'+ item.city_name +'">'+item.type+' '+item.city_name+'</option>');
              });
            }

            $('#city').trigger("chosen:updated");

          });

        });        

</script>