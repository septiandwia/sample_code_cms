<!DOCTYPE html>
<html>

<head>
    <meta charset="utf8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <title>Online Shop Panel Management</title>
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/bootstrap-2.css">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/bootstrap-2.responsive.min.css">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet" integrity="sha384-T8Gy5hrqNKT+hzMclPo118YTQO6cYprQmhrYwIiQ/3axmI1hQomh7Ud2hPOy8SP1" crossorigin="anonymous">
    <link rel="stylesheet" href="<?php echo $this->uri->baseUri;?>assets/admin/css/style.css">            <!--[if lt IE 9]>
    <script src="//css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
    <script src="//html5shiv.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
    <script src="<?php echo $this->uri->baseUri;?>assets/admin/js/jquery.1-8-2.min.js" type="text/javascript"></script>
    <script src="<?php echo $this->uri->baseUri;?>assets/admin/js/featherlight.js" type="text/javascript"></script>
</head>

<body>
    <div class="navbar navbar-static-top" style="border-top: 2px solid #46b8da;">
        <div class="navbar-inner">
            <div class="container-fluid">
                <div class="pull-left">
                    <button type="button" class="btn btn-navbar" data-toggle="offcanvas">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                </div>
                <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="brand" href="#" style="color: #46b8da;">Admin</a>
                <div class="nav-collapse collapse">
                    <ul class="nav pull-right">
                        <li>
                            <a style="padding: 5px 0 0 0;" href="#">
                            </a>
                        </li>
                        <li class="divider-vertical"></li>
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">Profile <b class="caret"></b></a>
                            <ul class="dropdown-menu">
                                <li>
                                    <a href="<?=$this->location('admin/password')?>">Change password</a>
                                </li>
                                <li class="divider"></li>
                                <li>
                                    <a href="<?php echo $this->location('admin/auth/logout'); ?>">Sign out</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row row-offcanvas row-offcanvas-left">
        <?php $this->output('_menu'); ?>