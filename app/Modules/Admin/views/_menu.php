<div class="span2 sidebar-offcanvas" id="sidebar" role="navigation">
    <div class="well sidebar-nav">
        <a class="btn btn-info" href="<?php echo $this->location('admin/product/edit'); ?>" style="margin-bottom: 20px;">Add Product</a>
        <ul class="nav nav-list">
            <li class="nav-header <?php echo ($active == 'dashboard') ? 'active' : '' ?> "><a href="<?php echo $this->location('admin/dashboard'); ?>"><i class="fa fa-home"></i> Overview</a></li>
            <li class="nav-header <?php echo ($active == 'product') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/product'); ?>"><i class="fa fa-gift"></i> Products</a></li>
            <?php if($active == 'product'): ?>
                <li class=""><a href="<?php echo $this->location('admin/product/edit'); ?>">Add Product</a></li>
            <?php endif; ?>
            <li class="nav-header <?php echo ($active == 'category') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/category'); ?>"><i class="fa fa-tags fa fa-2x"></i> Categories</a></li>
            <li class="nv-payment nav-header <?php echo ($active == 'payment') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/payment'); ?>"><i class="fa fa-credit-card"></i> Payment</a></li>
            <li class="nav-header <?php echo ($active == 'shipping') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/shipping'); ?>"><i class="fa fa-truck"></i> Shipping</a></li>
            <li class="nav-header <?php echo ($active == 'order') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/order'); ?>"><i class="fa fa-shopping-cart"></i> Orders</a></li>
            <li class="nav-header <?php echo ($active == 'page') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/page'); ?>"><i class="fa fa-copy fa fa-2x"></i> Pages</a></li>
            <li class="nav-header <?php echo ($active == 'template') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/template'); ?>"><i class="fa fa-fire-extinguisher"></i> Template</a></li>
            <li class="nav-header <?php echo ($active == 'setting') ? 'active' : '' ?>"><a href="<?php echo $this->location('admin/setting'); ?>"><i class="fa fa-wrench"></i> Settings</a></li>
        </ul>
        <hr>
    </div>
</div>
<div class="span10 main-content">