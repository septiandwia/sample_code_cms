<?php
namespace Modules\Admin\Controllers;
use Resources;

class Category extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->category  = new \Modules\Admin\Models\Category;
	}

	public function index() {

		$data['categories'] = $this->category->readAllCategory();
		$data['categories'] = $this->helper->showMenu($data['categories']);
		$data['active'] = 'category';

		$this->output('_header', $data);     
		$this->output('partial/category_home');
		$this->output('_footer');
	
	}

	public function edit() {

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			// JIKA KATEGORI DI EDIT / UPDATE
			if(isset($_POST['id'])) {
				$_POST['kategori_seo'] = $this->helper->setSeoTitle($_POST['name']);
				$_POST['kategori_seo'] = $this->cekSlug($_POST['kategori_seo']);				
				$input = $this->category->updateCategory((int)$_POST['id'], $_POST);
				$this->outputJSON($input);
				die;				
			}
			
			$_POST['kategori_seo'] = $this->helper->setSeoTitle($_POST['name']);
			$_POST['kategori_seo'] = $this->cekSlug($_POST['kategori_seo']);
			$input = $this->category->inputCategory($_POST);
			$this->outputJSON($input);
			die;

		}

		$data['categories'] = $this->category->readAllCategory();	
		// $data['categories'] = $this->helper->showUnlimitedMenuAdd($data['categories']);
		$data['categories'] = $this->helper->showStandardMenuAdd($data['categories']);

		// JIKA CATEGORY DI EDIT
		if(isset($_GET['id'])) {
			$data['category'] = $this->category->readCategoryById((int)$_GET['id']);
			$data['categories'] = $this->category->readAllCategory();
			$data['categories'] = array_values($data['categories'][0]);
		}

		$data['active'] = 'category';

		$this->output('_header', $data);     
		$this->output('partial/category_edit');
		$this->output('_footer');		
	
	}

	public function delete() {
		$id = (int)$_GET['id'];
		$result = $this->category->deleteCategory($id);

		if($result)
		$this->outputJSON(array('id' => $id));
	}

	// METHOD UNTUK MENGECEK SLUG BIAR UNIQUE
	public function cekSlug($slug) {
		$i = 1; $baseSlug = $_POST['kategori_seo'];
		while ($this->category->slug_exist($slug)) {
			$slug = $baseSlug . "-" . $i++;
		}
		return $slug;
	}	

}