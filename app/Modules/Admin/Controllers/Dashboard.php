<?php
namespace Modules\Admin\Controllers;
use Resources;

class Dashboard extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->kustomer = new \Modules\Admin\Models\Kustomer;
		$this->order 	= new \Modules\Admin\Models\Order;
	}

	public function index() {

		$data['jmlKustomer'] = $this->kustomer->countKustomer();
		$data['getTotalOrder'] = $this->order->myTotalOrder();
		$data['getTotalSalesMonth'] = $this->order->getTotalSalesMonth();
		$data['getTotalSalesMonth'] = number_format($data['getTotalSalesMonth'], 0, ",",".");
		$data['getTotalHarga'] = $this->order->myTotalHarga();
		$data['getTotalHarga'] = number_format($data['getTotalHarga'], 0, ",",".");
		$data['latestOrder'] = $this->order->latestOrder(5);
		$data['getTotalPerDay'] = ($this->order->getTotalPerDay() == false) ? 0 : $this->order->getTotalPerDay();

		if($data['getTotalPerDay'] != 0) {
			foreach($data['getTotalPerDay'] as $d) {
				$data['jmlDayOrder'][] = (int)$d->jumlah;
				$data['dayOrder'][] = date("j M", strtotime($d->tgl_order));
			}

			$data['jmlDayOrder'] = json_encode($data['jmlDayOrder']);
			$data['dayOrder'] = json_encode($data['dayOrder']);
		}

		$data['active'] = 'dashboard';

		$this->output('_header', $data);     
		$this->output('partial/home');
		$this->output('_footer');
	
	}

}