<?php
namespace Modules\Admin\Controllers;
use Resources;

class Order extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->order  = new \Modules\Admin\Models\Order;
		$this->kustomer  = new \Modules\Admin\Models\Kustomer;
	}

	public function index($page = 1) {

		$this->pagination = new Resources\Pagination();

		$page  = (isset($_GET['page'])) ? (int)$_GET['page'] : (int)$page;
		$limit = 10;

		$data['orders']   = $this->order->readAllOrder($page, $limit);
		$data['pageLinks'] = $this->pagination
			->setOption(
				array(
					'limit'		=> $limit,
					'base'		=> $this->uri->baseUri.'admin/order?page=%#%',
					'total'		=> $this->order->myTotalOrder(),
					'current'	=> $page
				)
			)
			->getUrl();						
		$data['active'] = 'order';

		$this->output('_header', $data);     
		$this->output('partial/order_home');
		$this->output('_footer');
	
	}

	public function detail() {

		$id = (int)$_GET['id'];

		$data['order'] = $this->order->detailOrder($id);
		$data['kustomer'] = $this->kustomer->detailKustomer($data['order'][0]->id_kustomer);
		$data['order'] = $data['order'][0];

		$data['id_order'] = $data['order']->id_orders;
		$data['tgl_order'] = date("j F Y - g:i a", strtotime($data['order']->tgl_order));
		$data['status_order'] = $data['order']->status_order;
		$data['total_harga'] = $data['order']->total_harga;
		$data['jenisPengString'] = $data['order']->jenis_pengiriman;
		$data['nama_kustomer'] = $data['kustomer'][0]->nama_lengkap;
		$data['nama_perusahaan'] = $data['kustomer'][0]->nama_perusahaan;
		$data['alamatFull'] = unserialize($data['order']->alamat_pengiriman);
		$data['alamat'] = $data['alamatFull']['alamat'];
		$data['alamat_ongkir'] = json_decode($data['alamatFull']['alamat_ongkir']);
		$data['alamat_ongkir'] = $data['alamat_ongkir']->rajaongkir->results;
		$data['telpon'] = $data['kustomer'][0]->telpon;
		$data['email'] = $data['kustomer'][0]->email;
		$data['details'] = unserialize($data['order']->details);
		$data['jenisPengString'] = explode("Rp.", $data['jenisPengString']);
		$data['jenis_pengiriman'] = $data['jenisPengString'][0];
		$data['harga_pengiriman'] = (int)str_replace(".", "", str_replace(" ", "", $data['jenisPengString'][1]));

		unset($data['order'], $data['kustomer'], $data['details']['cart_total'], $data['details']['total_items']);

		$data['active'] = 'order';
		
		$this->output('_header', $data);     
		$this->output('partial/order_detail');
		$this->output('_footer');	
	
	}

    public function getAddress($id_city = 39, $id_province = 5) {
        
        $this->ongkir   = new \Libraries\ongkir;
        
        $data = $this->ongkir->getAddress($id_city, $id_province);

        header('Content-Type: application/json');
        echo $data;

    }

    public function edit() {

    	$id = (int)$_GET['id'];

    	$quantity = $this->order->getQuantity($id);
    	foreach($quantity as $q) {
    		// MENGURANGI STOK PRODUK
    		$this->order->removeQuantity(array('id' => $q['id'], 'quantity' => $q['quantity']));
    	}

    	if (isset($_GET['status']) && $_GET['status'] == 'lunas')
    	{
    		$results = $this->order->updateStatus($id, $_GET['status']);
    		
    		if ($results) {
    			$this->redirect('admin/order/detail?id='.$id);
    		}
    	}
    }

}