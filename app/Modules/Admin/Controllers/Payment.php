<?php
namespace Modules\Admin\Controllers;
use Resources;

class Payment extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');
		
		$this->payment  = new \Modules\Admin\Models\Payment;	
	}

	public function index() {

		$data['rek'] 	= $this->payment->readPayment();
		$data['active'] = 'payment';

		$this->output('_header', $data);     
		$this->output('partial/payment_home');
		$this->output('_footer');
	
	}

	public function delete() {

		$id = (int)$_GET['id'];

		if($this->payment->deletePayment($id))
		$this->outputJSON(array('id' => $id));		

	}

	public function edit() {

		$data['active'] = 'payment';
		
		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$this->upload = new Resources\Upload;

			$_POST['fileName'] = $_FILES['logoBank']['name'];
			
			$insert = $this->payment->addPayment($_POST);

			if($insert) {
	
				$this->upload->setOption('folderLocation', './upload/files');
				
				$data['messages'] = "";

				if(isset($_FILES['logoBank'])) {

					$file = $this->upload->now($_FILES['logoBank']);
				    
				    if($file) {
					
						$data['messages'] = $this->upload->getFileInfo();
				    
				    }
				    else {
					
						$data['messages'] = $this->upload->getError('message');
				    
				    }			
				
				}
	
			}

			$this->redirect('admin/payment');

		}

		$this->output('_header', $data);     
		$this->output('partial/payment_edit');
		$this->output('_footer');		

	}

}