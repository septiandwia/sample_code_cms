<?php
namespace Modules\Admin\Controllers;
use Resources;

class Upload extends Resources\ControllerBaru {

	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->upload = new Resources\Upload;	
	}

	public function index() {

		$this->upload->setOption('folderLocation', './upload/'.date('Y').'/'.date('m').'/');
		$this->upload->setOption(
				array(
					'folderLocation' => './upload/'.date('Y').'/'.date('m').'/',
					'autoRename' =>true,
					'autoCreateFolder' => true
				)
			);

		$data['message'] = '';

		if( isset($_FILES['files']) ) {

		    $file = $this->upload->now($_FILES['files']);

		    if($file) {
				$this->outputJSON(
					$this->upload->getFileInfo()
				);
		    }
		    else {	
		    	echo "<pre>"; print_r($this->upload->getError('message')); echo "</pre>";
		    }
		}		 
	
	}

	public function delete() {
		
		$data = explode('upload', $_POST['data']);
		unlink('./upload/'.$data[1]);

		$this->outputJSON(array('file' => $data[1]));
	
	}

}