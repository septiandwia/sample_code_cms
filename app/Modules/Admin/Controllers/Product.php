<?php
namespace Modules\Admin\Controllers;
use Resources;

class Product extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->product  = new \Modules\Admin\Models\Product;
		$this->category  = new \Modules\Admin\Models\Category;
	}

	public function index($page = 1) {

		$this->pagination = new Resources\Pagination();

		$page  = (isset($_GET['page'])) ? (int)$_GET['page'] : (int)$page;
		$limit = 10;

		$data['products']   = $this->product->readAllProduct($page, $limit);
		$data['pageLinks'] = $this->pagination
			->setOption(
				array(
					'limit'		=> $limit,
					'base'		=> $this->uri->baseUri.'admin/product?page=%#%',
					'total'		=> $this->product->myTotalProduct(),
					'current'	=> $page
				)
			)
			->getUrl();
		$data['active'] = 'product';

		$this->output('_header', $data);     
		$this->output('partial/product_home', $data);
		$this->output('_footer');
	
	}

	public function edit() {

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			if(isset($_POST['update'])){
				
				if(isset($_POST['foto']))
					$_POST['foto'] = serialize($_POST['foto']);

				$input = $this->product->updateProduct($_POST);	

			} else {
				
				$_POST['foto'] 			= serialize($_POST['foto']);				
				$_POST['produk_seo']	= $this->helper->setSeoTitle($_POST['prod-name']);
				$_POST['produk_seo']	= $this->cekSlug($_POST['produk_seo']);
				$_POST['deskripsi'] 	= htmlspecialchars($_POST['deskripsi'],ENT_QUOTES);

				$input = $this->product->inputProduct($_POST);
			
			}

			$this->outputJSON($input);
			die;
		}

		// JIKA PRODUCT DI EDIT
		if(isset($_GET['id'])) {
			$data['produk'] = $this->product->readProduct((int)$_GET['id']);
			$data['produk'] = $data['produk'][0];
			$data['produk']->gambar = unserialize($data['produk']->gambar);
		}

		$data['categories'] = $this->category->readAllCategoryFront();
		$data['categories'] = $this->helper->generateSubMenu($data['categories']);

		$data['active'] = 'product';

		$this->output('_header', $data);     
		$this->output('partial/product_edit');
		$this->output('_footer');

	}

	public function delete() {

		$id = (int)$_GET['id'];		
		$input = $this->product->deleteProduct($id);
		
		if($input) $this->outputJSON(array('id' => $id));
	
	}

	// METHOD UNTUK MENGECEK SLUG BIAR UNIQUE
	public function cekSlug($slug) {
		$i = 1; $baseSlug = $_POST['produk_seo'];
		while ($this->product->slug_exist($slug)) {
			$slug = $baseSlug . "-" . $i++;
		}
		return $slug;
	}

}