<?php
namespace Modules\Admin\Controllers;
use Resources;

class Page extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->page = new \Modules\Admin\Models\Page;
	}

	public function index() {

		$data['pages'] = $this->page->readAllPage();		
		$data['active'] = 'page';

		$this->output('_header', $data);     
		$this->output('partial/page_home');
		$this->output('_footer');
	
	}

	public function edit($id = null) {

		if($id != null) {
			$id = (int)$id;			
			$data['page'] = $this->page->readPage($id);
			$data['page'] = $data['page'][0];
		}

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$_POST['content'] 	= $_POST['content'];
			$_POST['judul_seo']	= $this->helper->setSeoTitle($_POST['title']);
			$_POST['username']	= $_SESSION['username'];
			$_POST['isPublished'] = (isset($_POST['isPublished'])) ? 1 : 0;

			if(isset($_POST['id_berita'])) {
				$insert = $this->page->updatePage($_POST);
			} else {
				$insert = $this->page->addPage($_POST);
			}
				
			$this->outputJSON($_POST);
			die;

		}

		$data['active'] = 'page';

		$this->output('_header', $data);     
		$this->output('partial/page_edit');
		$this->output('_footer');		

	}

	public function delete() {

		$id = (int)$_GET['id'];		
		$input = $this->page->deletePage($id);
		
		if($input) $this->outputJSON(array('id' => $id));
	
	}

	public function dontpublish() {
		$id = (int)$_GET['id'];
		$data['publish'] = 0;
		$input = $this->page->updatePublish($id, $data);

		if($input) $this->outputJSON(array('id' => $id));
	}

	public function publish() {
		$id = (int)$_GET['id'];
		$data['publish'] = 1;
		$input = $this->page->updatePublish($id, $data);

		if($input) $this->outputJSON(array('id' => $id));
	}

}