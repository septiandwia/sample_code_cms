<?php
namespace Modules\Admin\Controllers;
use Resources;

class Setting extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->setting  = new \Modules\Admin\Models\Setting;
	}

	public function index() {

		$data['setting'] = $this->setting->read();
		$data['active'] = 'setting';

		$this->output('_header', $data);     
		$this->output('partial/setting_home');
		$this->output('_footer');
	
	}

	public function update() {
		
		$_POST['description'] = htmlspecialchars($_POST['description'],ENT_QUOTES);
		
		$update = $this->setting->update($_POST);
		
		if($update) {
			$this->outputJSON($_POST);
			die;				
		}
	}

}