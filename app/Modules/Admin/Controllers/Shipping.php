<?php
namespace Modules\Admin\Controllers;
use Resources;

class Shipping extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->ongkir   = new \Libraries\ongkir;
		$this->setting  = new \Modules\Admin\Models\Setting;
	}

	public function index() {

		if($_SERVER['REQUEST_METHOD'] == 'POST') {

			$city = explode('-', $_POST['city']);
			$cityName 	= $city[0];
			$cityId 	= $city[1];

			$result = $this->setting->updateCity($cityId, $cityName);

		}

		$data['active'] = 'shipping';
		$data['cityName'] = ($this->setting->getCityName()) ? get_object_vars($this->setting->getCityName()[0]) : 'Kota belum di set';

		$this->output('_header', $data);     
		$this->output('partial/shipping_home');
		$this->output('_footer');
	
	}

}