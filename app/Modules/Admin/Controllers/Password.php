<?php
namespace Modules\Admin\Controllers;
use Resources;

class Password extends Resources\ControllerBaru {

	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->setting = new \Modules\Admin\Models\Setting;
	}

	public function index() {

		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			$_POST['password'] = $this->request->post('password', FILTER_SANITIZE_STRING);
			$_POST['password'] = md5($_POST['password']);
			
			$this->setting->updatePassword($_POST['password']);
			
			$data['status'] = "PASSWORD BERHASIL DIRUBAH!";
		}

		$data['active'] = 'password';
		
		$this->output('_header', $data);     
		$this->output('partial/password_home');
		$this->output('_footer');		
	
	}

}