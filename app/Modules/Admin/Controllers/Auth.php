<?php
namespace Modules\Admin\Controllers;
use Resources, Modules\Admin;

class Auth extends Resources\ControllerBaru {

	public function __construct(){
		parent::__construct();
	}

	public function index() {
		$this->redirect('admin');
	}

    public function login()
    {

    	if(isset($_POST['username']) AND isset($_POST['password'])) {

    		$this->loginUser = new \Libraries\auth;
            
            $username = $this->request->post('username', FILTER_SANITIZE_STRING);
            $password = $this->request->post('password', FILTER_SANITIZE_STRING);

            if( $this->loginUser->cekLogin($username, md5($password), $table='admins') ) {
                $this->redirect('admin/dashboard');
            } else {
                // $data['erMessage'] = "Username atau Password salah!";
                $this->redirect('admin');                
            }

        } else {
            $this->redirect('admin');
        }
    }

    public function logout()
    {
    	session_destroy();
    	$this->redirect('admin');
    }

}