<?php
namespace Modules\Admin\Controllers;
use Resources;

class Template extends Resources\ControllerBaru {
	
	public function __construct() {
		parent::__construct();
		
		if(!$this->access->accessLogin())
			$this->redirect('admin');

		$this->minify = new \Libraries\minifier;
        $this->loader   = new \Twig_Loader_Filesystem('./template'); //LOKASI TEMPLATE YANG DIGUNAKAN
        $this->twig     = new \Twig_Environment($this->loader);
        $this->page     = new \Modules\Admin\Models\Page;
        $this->product  = new \Modules\Admin\Models\Product;
        $this->category = new \Modules\Admin\Models\Category;
        $this->setting  = new \Modules\Admin\Models\Setting;
        $this->cart     = new \Resources\Cart;
        $this->bank     = new \Modules\Admin\Models\Payment;        
        $data['bank']   = $this->bank->readPayment();
        $data['categories'] = $this->category->readAllCategoryFront();
        $data['categories'] = $this->helper->generateSubMenu($data['categories']);

        $this->baseConfig = array(

            'template'   => $this->uri->baseUri . 'template/', //LOKASI ASSETS TEMPLATE
            'root'       => $this->uri->baseUri . 'index.php/',
            'categories' => $data['categories'],
            'banks'       => $data['bank']
        
        );               		
	}

	public function index() {
		
		$data['active'] = 'template';

		$this->output('_header', $data);     
		$this->output('partial/template_home');
		$this->output('_footer');
	
	}

	public function docs() {
		
		$data['active'] = 'template';

		$this->output('_header', $data);     
		$this->output('partial/template_doc');
		$this->output('_footer');

	}

	public function editor() {

		$this->template = $this->twig->loadTemplate('index.html');

		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			file_put_contents('./template/index.html', $_POST['data']);
		}

		$data['active'] = 'template';
		$data['template'] = htmlspecialchars(file_get_contents('./template/index.html'));

		$this->output('partial/template_edit', $data);
	
	}

	public function reset() {
	
		$data = file_get_contents('./template/default.html');

		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			file_put_contents('./template/index.html', $data);
		}

		$this->outputJSON(array('data' => true));

	}

	public function preview() {

		if(isset($_GET['removePreview'])) {
			if(isset($_SESSION['preview'])){
				unset($_SESSION['preview']);
				return true;
			}
			else { return false; }
		}

		$this->template = $this->twig->loadTemplate('preview.html');

		if($_SERVER['REQUEST_METHOD'] == 'POST') {
			file_put_contents('./template/preview.html', $_POST['data']);
			$_SESSION['preview'] = true;
		}

		$r['produk']   	= $this->product->readAllProduct($page = 1, $limit = 12);

		foreach($r['produk'] as $key => $v) {
			$r['produk'][$key]->gambar = unserialize($v->gambar);
			$r['produk'][$key]->thumbnail = $r['produk'][$key]->gambar[0];
		}

		$data = array(
					'root' => $this->baseConfig['root'],
					'template' => $this->baseConfig['template'],
					'banks' => $this->baseConfig['banks'],
					'categories' => $this->baseConfig['categories'],
					'page' => $this->page->readAllPage('publish'),
					'produk' => $r['produk'],
					'homePage' => true,
					'cartItems' => $this->cart->total_items(),
					'cartTotal' => $this->cart->total(),
					'setting' => $this->setting->read()
				);

		echo $this->template->render($data);		
	}

}