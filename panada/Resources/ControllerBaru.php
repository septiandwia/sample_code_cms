<?php

namespace Resources;

class ControllerBaru extends Controller {

    function __construct() {
        
        parent::__construct();
        Import::composer();

		$this->session  = new \Resources\Session;
		$this->request  = new \Resources\Request;        
        $this->access   = new \Libraries\access;
        $this->helper   = new \Libraries\helper;
            
    }

}