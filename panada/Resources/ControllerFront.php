<?php

namespace Resources;

class ControllerFront extends Controller {

    public function __construct(){

        parent::__construct();
        Import::composer();

        $this->page     = new \Modules\Admin\Models\Page;
        $this->product  = new \Modules\Admin\Models\Product;
        $this->category = new \Modules\Admin\Models\Category;
        $this->setting  = new \Modules\Admin\Models\Setting;
        $this->helper   = new \Libraries\helper;
        $this->loader   = new \Twig_Loader_Filesystem('./template'); //LOKASI TEMPLATE YANG DIGUNAKAN
        $this->twig     = new \Twig_Environment($this->loader);
        $this->session  = new \Resources\Session;
        $this->request  = new \Resources\Request;
        $this->cart     = new \Resources\Cart;
        $this->bank     = new \Modules\Admin\Models\Payment;
        $this->limit    = 12;

        if(isset($_SESSION['preview']))
        $this->template = $this->twig->loadTemplate('preview.html');
        else
        $this->template = $this->twig->loadTemplate('index.html');

        $data['bank']   = $this->bank->readPayment();
        $data['categories'] = $this->category->readAllCategoryFront();
        $data['categories'] = $this->helper->generateSubMenu($data['categories']);

        // PAKAI YANG INI JIKA MAU SUBMENU UNLIMITED
        // $data['categories'] = $this->helper->showMenuTemplate($data['categories']);        

        $this->baseConfig = array(

            'template'   => $this->uri->baseUri . 'template/', //LOKASI ASSETS TEMPLATE
            'root'       => $this->uri->baseUri . 'index.php/',
            'categories' => $data['categories'],
            'banks'       => $data['bank']
        
        );


    }

}    